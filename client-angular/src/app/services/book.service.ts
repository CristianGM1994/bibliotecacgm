import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GLOBAL} from './global';
import {Book} from '../models/book';
import { UserService } from './user.service';
import { Mail } from '../models/mail';
import { RecordPage } from '../models/recordpage';

@Injectable()
export class BookService
{
	public url: string;
	public token;
	public identity;

	constructor(public _http: HttpClient, private _userService: UserService)
	{
		this.url = GLOBAL.url;
		this.identity = this._userService.getIdentity();
	}

	pruebas()
	{
		return "Hola mundo";
	}

	create(token, book: Book): Observable<any>
	{
		console.log(this.identity);
		let json = JSON.stringify(book);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		if(this.identity.role == 'ROLE_ADMIN')
		{
			return this._http.post(this.url+'books', params, {headers:headers});
		}
		else
		{
			return this._http.post(this.url+'revisionbooks', params, {headers:headers});
		}
	}

	getBooks(): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return this._http.get(this.url+"books", {headers:headers});
	}

	getBook(id): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return this._http.get(this.url+"books/"+id, {headers:headers});
	}

	destroy(token, id): Observable<any>
	{
		let json = JSON.stringify(id);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);


		return this._http.delete(this.url+'books/'+id, {headers:headers});
	}

	consultarlibro(tipo, nombre)
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return this._http.get(this.url+"findbooks/"+tipo+"/"+nombre, {headers:headers});
	}

	orderbook(tipo, orden): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return this._http.get(this.url+"books/orderbook/"+tipo+"/"+orden, {headers:headers});
	}

	mail(token, mail: Mail): Observable<any>
	{
		let json = JSON.stringify(mail);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		
		return this._http.post(this.url+'sendmail', params, {headers:headers});

	}

	recordNumPages(token, recordPage: RecordPage): Observable<any>
	{
		let json = JSON.stringify(recordPage);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		
		return this._http.post(this.url+'books/recordpage', params, {headers:headers});
	}

	getNumPages(token, identidad, libro): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.get(this.url+"books/recordpage/"+identidad+"/"+libro, {headers:headers});
	}

	deleteNumPages(token, recordPage: RecordPage): Observable<any>
	{
		let json = JSON.stringify(recordPage);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		
		return this._http.post(this.url+'books/deleterecordpage', params, {headers:headers});
	}
}
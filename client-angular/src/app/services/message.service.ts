import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GLOBAL} from './global';
import {Message} from '../models/message';
import { UserService } from './user.service';

@Injectable()
export class MessageService
{
	public url: string;
	public token;
	public identity;

	constructor(public _http: HttpClient, private _userService: UserService)
	{
		this.url = GLOBAL.url;
		this.identity = this._userService.getIdentity();
	}

	pruebas()
	{
		return "Hola mundo";
	}

	create(token, message: Message): Observable<any>
	{

		let json = JSON.stringify(message);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.post(this.url+'message', params, {headers:headers});
	}

	index(token, id): Observable<any>
	{
		let json = JSON.stringify(id);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.get(this.url+'messageindex/'+id, {headers:headers});
	}

	delete(token, id): Observable<any>
	{
		let json = JSON.stringify(id);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.get(this.url+'deletemessage/'+id, {headers:headers});
	}

	notseemessage(token, id): Observable<any>
	{
		let json = JSON.stringify(id);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.get(this.url+'notseemessage/'+id, {headers:headers});
	}

	show(token, id): Observable<any>
	{
		let json = JSON.stringify(id);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.get(this.url+'showmessage/'+id, {headers:headers});
	}

	define(token, id): Observable<any>
	{
		let json = JSON.stringify(id);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.get(this.url+'definemessage/'+id, {headers:headers});
	}
}
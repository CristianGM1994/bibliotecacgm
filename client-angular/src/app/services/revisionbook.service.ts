import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GLOBAL} from './global';
import {RevisionBook} from '../models/revisionbook';
import { UserService } from './user.service';

@Injectable()
export class RevisionBookService
{
	public url: string;
	public token;
	public identity;

	constructor(public _http: HttpClient, private _userService: UserService)
	{
		this.url = GLOBAL.url;
		this.identity = this._userService.getIdentity();
	}

	pruebas()
	{
		return "Hola mundo";
	}

	getBooks(): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return this._http.get(this.url+"revisionbooks", {headers:headers});
	}

	update(token, id): Observable<any>
	{
		let json = JSON.stringify(id);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.put(this.url+'revisionbooks/'+id, params, {headers:headers});
	}

	destroy(token, id): Observable<any>
	{
		let json = JSON.stringify(id);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);


		return this._http.delete(this.url+'revisionbooks/'+id, {headers:headers});
	}

	orderbook(tipo, orden): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return this._http.get(this.url+"books/orderrevisionbook/"+tipo+"/"+orden, {headers:headers});
	}
}
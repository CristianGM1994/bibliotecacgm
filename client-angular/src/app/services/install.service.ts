import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GLOBAL} from './global';
import {User} from '../models/user';

@Injectable()
export class InstallService
{
	public url: string;
	public identity;
	public token;

	constructor(public _http: HttpClient)
	{
		this.url = GLOBAL.url;
    }

    getbbdd(): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return this._http.get(this.url+'getbd', {headers:headers});
	}

    setbbdd(bbdd): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return this._http.get(this.url+'install/'+bbdd, {headers:headers});
	}
}
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GLOBAL} from './global';
import {BookOnline} from '../models/bookonline';
import { UserService } from './user.service';

@Injectable()
export class BookOnlineService
{
    public url: string;
	public token;
	public identity;

	constructor(public _http: HttpClient, private _userService: UserService)
	{
		this.url = GLOBAL.url;
		this.identity = this._userService.getIdentity();
	}

	pruebas()
	{
		return "Hola mundo";
    }
    
    create(token, book: BookOnline): Observable<any>
	{
		console.log(this.identity);
		let json = JSON.stringify(book);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);


        return this._http.post(this.url+'booksonline', params, {headers:headers});
		
	}

	search(id): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return this._http.get(this.url+"booksonline/search/"+id, {headers:headers});
	}

	show(id): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return this._http.get(this.url+"booksonline/"+id, {headers:headers});
	}

	update(token, book, id): Observable<any>
	{
		let json = JSON.stringify(book);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.put(this.url+'booksonline/'+id, params, {headers:headers});
	}

	destroy(token, id): Observable<any>
	{
		let json = JSON.stringify(id);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);


		return this._http.delete(this.url+'booksonline/'+id, {headers:headers});
	}

	sendrevision(token, id): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.get(this.url+"booksonline/sendrevision/"+id, {headers:headers});
	}


	updatebookonline(token, book, id): Observable<any>
	{
		let json = JSON.stringify(book);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.put(this.url+'booksonline/updatebookonline/'+id, params, {headers:headers});
	}
}
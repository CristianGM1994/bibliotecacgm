import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GLOBAL} from './global';
import {Book} from '../models/book';
import {Comment} from '../models/comment';
import { UserService } from './user.service';

@Injectable()
export class CommentService
{
	public url: string;
	public token;
	public identity;

	constructor(public _http: HttpClient, private _userService: UserService)
	{
		this.url = GLOBAL.url;
		this.identity = this._userService.getIdentity();
	}

	pruebas()
	{
		return "Hola Comments";
	}

	getComments(id): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return this._http.get(this.url+"comments/"+id, {headers:headers});
	}

	create(token, comment: Comment): Observable<any>
	{

		let json = JSON.stringify(comment);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.post(this.url+'comments', params, {headers:headers});
	}

	delete(token, id): Observable<any>
	{
		let json = JSON.stringify(id);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.get(this.url+'deletecomment/'+id, {headers:headers});
	}
}
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GLOBAL} from './global';
import {Genre} from '../models/genre';

@Injectable()
export class GenreService
{
	public url: string;

	constructor(public _http: HttpClient)
	{
		this.url = GLOBAL.url;
	}

	pruebas()
	{
		return "Hola genre";
	}

	getGenre(): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return this._http.get(this.url+"genre", {headers:headers});
	}

	create(token, genre: Genre): Observable<any>
	{
		let json = JSON.stringify(genre);
		let params = 'json=' + json;

		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);


		return this._http.post(this.url+'genre', params, {headers:headers});
	}
}
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GLOBAL} from './global';
import {Book} from '../models/book';
import {Like} from '../models/like';
import { UserService } from './user.service';

@Injectable()
export class LikeService
{
	public url: string;
	public token;
	public identity;

	constructor(public _http: HttpClient, private _userService: UserService)
	{
		this.url = GLOBAL.url;
		this.identity = this._userService.getIdentity();
	}

	pruebas()
	{
		return "Hola mundo";
	}

	create(token, userid, bookid): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.get(this.url+"likes/"+userid+"/"+bookid, {headers:headers});
	}

	destroy(token, userid, bookid): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.get(this.url+"likes/destroy/"+userid+"/"+bookid, {headers:headers});
	}

	getLikes(token, userid): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.get(this.url+"likes/"+userid, {headers:headers});
	}

	showLikes(token, userid): Observable<any>
	{
		let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
		.set('Authorization', token);

		return this._http.get(this.url+"likes/show/"+userid, {headers:headers});
	}
}
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


//Componentes

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { HomeComponent } from './components/home/home.component';
import { BookNewComponent } from './components/book-new/book-new.component';
import { BookDetailComponent } from './components/book-detail/book-detail.component';
import { BookListComponent } from './components/book-list/book-list.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { UserLikeComponent } from './components/user-like/user-like.component';
import { UserSendMessageComponent } from './components/user-send-message/user-send-message.component';
import { UserReceiveMessageComponent } from './components/user-receive-message/user-receive-message.component';
import { UserDeleteMessageComponent } from './components/user-delete-message/user-delete-message.component';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { AddGenreComponent } from './components/add-genre/add-genre.component';
import { UserSeeMessageComponent } from './components/user-see-message/user-see-message.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserDeleteComponent } from './components/user-delete/user-delete.component';
import { RevisionBookListComponent } from './components/revision-book-list/revision-book-list.component';
import { RevisionBookDeleteComponent } from './components/revision-book-delete/revision-book-delete.component';
import { BookDeleteComponent } from './components/book-delete/book-delete.component';
import { FindBookComponent } from './components/find-book/find-book.component';
import { InstallComponent } from './components/install/install.component';
import { LogComponent } from './components/log/log.component';
import { UserDeleteCommentComponent } from './components/user-delete-comment/user-delete-comment.component';
import { SendMailComponent } from './components/send-mail/send-mail.component';
import { BookOnlineTableComponent } from './components/book-online-table/book-online-table.component';
import { BookOnlineNewComponent } from './components/book-online-new/book-online-new.component';
import { BookOnlineDeleteComponent } from './components/book-online-delete/book-online-delete.component';
import { BookOnlineWriteComponent } from './components/book-online-write/book-online-write.component';

const appRoutes: Routes = [

{path:'', component: InstallComponent},
{path:'login', component: LoginComponent},
{path:'logout/:sure', component: LoginComponent},
{path:'home', component: HomeComponent},
{path:'new', component: BookNewComponent},
{path:'booklist', component: BookListComponent},
{path:'bookdetail/:id', component: BookDetailComponent},
{path:'bookdelete/:id', component: BookDeleteComponent},
{path:'sendbookmail/:id', component: SendMailComponent},
{path:'useredit/:id', component: UserEditComponent},
{path:'userdetail/:id', component: UserDetailComponent},
{path:'userlike/:id', component: UserLikeComponent},
{path:'userdelete/:id', component: UserDeleteComponent},
{path:'usersendmessage', component: UserSendMessageComponent},
{path:'usersendmessage/:id', component: UserSendMessageComponent},
{path:'userdeletemessage/:idmessage/:id', component: UserDeleteMessageComponent},
{path:'userdeletecomment/:idcomment/:id', component: UserDeleteCommentComponent},
{path:'userseemessage/:idmessage', component: UserSeeMessageComponent},
{path:'userreceivemessage', component: UserReceiveMessageComponent},
{path:'admin', component: AdminPanelComponent},
{path:'addgenre', component: AddGenreComponent},
{path:'userlist', component: UserListComponent},
{path:'register', component: RegisterComponent},
{path:'revisionbooklist', component: RevisionBookListComponent},
{path:'revisionbookdelete/:id', component: RevisionBookDeleteComponent},
{path:'log', component: LogComponent},
{path:'findbook', component: FindBookComponent},
{path:'bookonlinetable', component: BookOnlineTableComponent},
{path:'bookonlinenew', component: BookOnlineNewComponent},
{path:'bookonlinenew/:id', component: BookOnlineNewComponent},
{path:'bookonlinedelete/:id', component: BookOnlineDeleteComponent},
{path:'writebookonline/:id', component: BookOnlineWriteComponent},
{path:'**', component: WelcomeComponent}

];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
import { Component, OnInit, DoCheck } from '@angular/core';
import { UserService } from './services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService]
})
export class AppComponent implements OnInit, DoCheck {
  title = 'client-angular';
  public identity;
  public token;
  public href: string = "";

  constructor(private _userService: UserService, private router: Router)
  {
  	this.identity = this._userService.getIdentity();
  	this.token = this._userService.getToken();
  }

  ngOnInit()
  {
    console.log('App.Component Cargado');
    this.href = this.router.url;
    console.log(this.href);
  }

  ngDoCheck()
  {
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.href = this.router.url;
  }
}

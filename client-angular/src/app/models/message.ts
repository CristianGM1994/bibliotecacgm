export class Message
{
      constructor(
      	public id: number,
        public asunto: string,
        public mensaje: string,
        public user_receive: number,
        public user_send: number
      )
      {

      }
}

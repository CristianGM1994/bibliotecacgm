export class Log
{
      constructor(
      	public user_id: number,
        public role: string,
        public activity: string,
        public created_at: string,
      )
      {

      }
}

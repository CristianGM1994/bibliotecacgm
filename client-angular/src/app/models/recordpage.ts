export class RecordPage
{
      constructor(
      	public id: number,
        public numpages: number,
        public created_at: string,
        public user_id: number,
        public book_id: number,
        public librofinalizado: boolean,
      )
      {

      }
}

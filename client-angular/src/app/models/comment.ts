export class Comment
{
      constructor(
      	public id: number,
        public comment: string,
        public created_at: string,
        public sub: number,
        public book_id: number
      )
      {

      }
}

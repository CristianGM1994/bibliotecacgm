export class BookOnline
{
      constructor(
            public id: number,
            public title: string,
            public autor: string,
            public description: string,
            public bookimage: string,
            public bookonline: string,
            public user_id: number,
            public genre_id: number
      )
      {

      }
}

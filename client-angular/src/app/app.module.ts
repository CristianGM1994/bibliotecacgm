import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { routing, appRoutingProviders } from './app.routing';
import {NgxPaginationModule} from 'ngx-pagination';
import { CKEditorModule } from 'ckeditor4-angular';

// Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { HomeComponent } from './components/home/home.component';
import { BookNewComponent } from './components/book-new/book-new.component';
import { BookDetailComponent } from './components/book-detail/book-detail.component';
import { BookListComponent } from './components/book-list/book-list.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { UserLikeComponent } from './components/user-like/user-like.component';
import { UserSendMessageComponent } from './components/user-send-message/user-send-message.component';
import { UserReceiveMessageComponent } from './components/user-receive-message/user-receive-message.component';
import { UserDeleteMessageComponent } from './components/user-delete-message/user-delete-message.component';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { AddGenreComponent } from './components/add-genre/add-genre.component';
import { UserSeeMessageComponent } from './components/user-see-message/user-see-message.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserDeleteComponent } from './components/user-delete/user-delete.component';
import { RevisionBookListComponent } from './components/revision-book-list/revision-book-list.component';
import { RevisionBookDeleteComponent } from './components/revision-book-delete/revision-book-delete.component';
import { BookDeleteComponent } from './components/book-delete/book-delete.component';
import { FindBookComponent } from './components/find-book/find-book.component';
import { InstallComponent } from './components/install/install.component';
import { LogComponent } from './components/log/log.component';
import { UserDeleteCommentComponent } from './components/user-delete-comment/user-delete-comment.component';
import { SendMailComponent } from './components/send-mail/send-mail.component';
import { BookOnlineTableComponent } from './components/book-online-table/book-online-table.component';
import { BookOnlineNewComponent } from './components/book-online-new/book-online-new.component';
import { BookOnlineDeleteComponent } from './components/book-online-delete/book-online-delete.component';
import { BookOnlineWriteComponent } from './components/book-online-write/book-online-write.component';

//Pipes
import { FilterPipe } from './pipes/filter.pipe';
import { TimeAgoPipe } from 'time-ago-pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    WelcomeComponent,
    HomeComponent,
    BookNewComponent,
    BookDetailComponent,
    BookListComponent,
    FilterPipe,
    TimeAgoPipe,
    UserEditComponent,
    UserDetailComponent,
    UserLikeComponent,
    UserSendMessageComponent,
    UserReceiveMessageComponent,
    UserDeleteMessageComponent,
    AdminPanelComponent,
    AddGenreComponent,
    UserSeeMessageComponent,
    UserListComponent,
    UserDeleteComponent,
    RevisionBookListComponent,
    RevisionBookDeleteComponent,
    BookDeleteComponent,
    FindBookComponent,
    InstallComponent,
    LogComponent,
    UserDeleteCommentComponent,
    SendMailComponent,
    BookOnlineTableComponent,
    BookOnlineNewComponent,
    BookOnlineDeleteComponent,
    BookOnlineWriteComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routing,
    NgxPaginationModule,
    CKEditorModule
  ],
  providers: [
    appRoutingProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

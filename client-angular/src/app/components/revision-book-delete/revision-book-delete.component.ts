import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { RevisionBook } from '../../models/revisionbook';
import { RevisionBookService } from '../../services/revisionbook.service';
import { Message } from '../../models/message';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-revision-book-delete',
  templateUrl: './revision-book-delete.component.html',
  styleUrls: ['./revision-book-delete.component.css'],
  providers: [UserService, RevisionBookService, MessageService]
})
export class RevisionBookDeleteComponent implements OnInit {

  public page_title: string;
  public identity;
  public token;
  public id;
  public message: Message;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _messageService: MessageService, private _revisionbookService: RevisionBookService) 
  {
  	this.page_title = 'Eliminar Revisión de Libro';
	this.identity = this._userService.getIdentity();
	this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
  	if(this.identity == null || this.identity.role != 'ROLE_ADMIN')
    {
        this._router.navigate(['home', { 'message': "No tienes permiso para visualizar esta sección", 'type': "danger" }]);
    }

  	this.getParams();
  }

  getParams()
  {
  		this._route.params.subscribe(params => {
  			this.id = params['id'];
  		});
  }

  destroy()
  {
    this._revisionbookService.destroy(this.token, this.id).subscribe(
          response => {
            if (response.status == 'success')
            {
                this.message = new Message(1, 'Novedades sobre su publicación', 'Le enviamos este mensaje para informarle de la desestimación sobre la solicitud de la obra enviada. La Administración ha encontrado contenido y/o no relacionado con la politica de la plataforma.', response.book.user_id, this.identity.sub);

                this._messageService.create(this.token, this.message).subscribe(
    			      response => 
    			      {
    			        if(response.status == 'success')
    			        {
    	                	this._router.navigate(['home', { 'message': "¡Libro eliminado con exito!", 'type': "success" }]);
    	                }
    	                else
    			        {
    			          console.log("Ocurrió un problema");
    			        }		        
    			      },
    			      error => {
    			        console.log(<any>error);
    			      }
    			     )
                }
                else
                {
                  console.log(response);
                }
          },
          error => {
            console.log(error);
          }
        );
  }

}

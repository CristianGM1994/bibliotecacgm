import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisionBookDeleteComponent } from './revision-book-delete.component';

describe('RevisionBookDeleteComponent', () => {
  let component: RevisionBookDeleteComponent;
  let fixture: ComponentFixture<RevisionBookDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevisionBookDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisionBookDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

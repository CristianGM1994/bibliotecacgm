import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { BookOnlineService } from '../../services/bookonline.service';
import { GenreService } from '../../services/genre.service';
import { BookOnline } from '../../models/bookonline';

@Component({
  selector: 'app-book-online-delete',
  templateUrl: './book-online-delete.component.html',
  styleUrls: ['./book-online-delete.component.css'],
  providers: [UserService, BookOnlineService]
})
export class BookOnlineDeleteComponent implements OnInit {

  public page_title: string;
  public identity;
  public token;
  public id;
  public book: BookOnline;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _bookonlineService: BookOnlineService) 
  {
    this.page_title = 'Eliminar Libro Online';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  ngOnInit() 
  {

    this.getParams();

    if(this.identity == null || this.book.user_id != this.identity.sub)
    {
        this._router.navigate(['home', { 'message': "No tienes permiso para visualizar esta sección", 'type': "danger" }]);
    }

  	
  }

  getParams()
  {
  		this._route.params.subscribe(params => {
  			this.id = params['id'];
      });
      
      this._bookonlineService.show(this.id).subscribe(
        response => 
        {
          if(response.status == 'success')
          {
              this.book = response.books;
              
              if(this.book.user_id != this.identity.sub)
              {
                this._router.navigate(['home', { 'message': "No tienes permiso para visualizar esta sección", 'type': "danger" }]);
              }

              console.log(this.book);
          }
          else
          {
            console.log("Algo salió mal");
          }      
        },
        error => {
          console.log(error);
        }
    )
  }

  destroy()
  {
    this._bookonlineService.destroy(this.token, this.id).subscribe(
          response => {
            if (response.status == 'success')
            {
              this._router.navigate(['home', { 'message': "Libro Online Eliminado Correctamente", 'type': "success" }]);
            }
            else
            {
              console.log(response);
            }
          },
          error => {
            console.log(error);
          }
        );
  }

  

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookOnlineDeleteComponent } from './book-online-delete.component';

describe('BookOnlineDeleteComponent', () => {
  let component: BookOnlineDeleteComponent;
  let fixture: ComponentFixture<BookOnlineDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookOnlineDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookOnlineDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, DoCheck } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { BookService } from '../../services/book.service';
import { CommentService } from '../../services/comment.service';
import { Book } from '../../models/book';
import { User } from '../../models/user';
import { Comment } from '../../models/comment';
import { LikeService } from '../../services/like.service';
import { Like } from '../../models/like';
import { RecordPage } from '../../models/recordpage';
import * as $ from 'jquery';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css'],
  providers: [UserService, BookService, CommentService, LikeService]
})
export class BookDetailComponent implements OnInit {

  public page_title: string;
  public identity;
  public token;
  public book: Book;
  public message: Array<Book>;
  public comments: Array<Comment>;
  public comment: Comment;
  public recordpage: RecordPage;
  public likelist;
  public numpage;
  public id;
 
  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _bookService: BookService, private _commentService: CommentService, private _likeService: LikeService) 
  {
  	  this.page_title = 'Listado de Libros';
  	  this.identity = this._userService.getIdentity();
      this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
  	if(this.identity == null)
    {
        this._router.navigate(['']);
    }
    else
    {
      this.comment = new Comment(1, '', '', 1, 1);
      this.recordpage = new RecordPage(0, 0, '', 1, 1, false); 
    }

    this.getBook();
    this.comment.sub = this.identity.sub;
		this.getComments();
		
    this.getLikes(this.identity.sub);

    this.getNumPages(this.token, this.identity.sub, this.id); 
  }

  getBook()
  {
	  	this._route.params.subscribe(params => {
	  		this.id = +params['id'];

	  		this._bookService.getBook(this.id).subscribe(
	  			response => {
	  				if (response.status == 'success')
	  				{
	  					this.book = response.books;
	  					console.log(response.books);
              this.comment.book_id = response.books.id;
	  				}
	  				else
	  				{
	  					this._router.navigate(['home', { 'message': "No se ha podido ejecutar la busqueda" }]);
	  				}
	  			},
	  			error => {
	  				this._router.navigate(['home', { 'message': "No se ha podido ejecutar la busqueda", 'type': "danger" }]);
	  			}
	  		);
	  	});
	}

	getPruebas()
	{
		console.log(this._commentService.pruebas());
	}

	getComments()
	{
		this._route.params.subscribe(params => {
	  		let id = +params['id'];

	  		this._commentService.getComments(id).subscribe(
	  			response => {
	  				if (response.status == 'success')
	  				{
	  					this.comments = response.comments;
	  				}
	  				else
	  				{
	  					console.log(response);
	  				}
	  			},
	  			error => {
	  				console.log(<any>error);
	  			}
	  		);
	  	});
  }
  
	onSubmit(form)
	{
		this._commentService.create(this.token, this.comment).subscribe(
        response => 
      	{
	        if(response.status == 'success')
	        {
						console.log(response);
						this.getComments();
	          form.reset();
	        }
	        else
	        {
	       	  console.log(this.comment);
	          console.log(response);
	        }
        
       },
      error => {
      console.log(this.comment);
        console.log(<any>error);
      }
     )
	}

	sendLike(id)
  {
    this._likeService.create(this.token, this.identity.sub, id).subscribe(
      response => 
      {
        if(response.status == 'success')
        {
            console.log(response);
            this.getLikes(this.identity.sub);   
        }
        else
        {
          console.log(response.error)
        }
        
      },
      error => {
        console.log(<any>error);
      }
     )
  }

  sendDislike(id)
  {
    this._likeService.destroy(this.token, this.identity.sub, id).subscribe(
      response => 
      {
        if(response.status == 'success')
        {
          console.log(response);
          console.log("Destruido");
          this.getLikes(this.identity.sub);  
        }
        else
        {
          console.log(response.error)
        }
        
      },
      error => {
        console.log(<any>error);
      }
     )
  }

  getLikes(id)
  {
    this._likeService.getLikes(this.token, id).subscribe(
      response => 
      {
        if(response.status == 'success')
        {
          this.likelist = response.likes;
          console.log(this.likelist);

        }
        else
        {
          console.log(response.error)
        }
        
      },
      error => {
        console.log(<any>error);
      }
     )
  }

  checkLike(id)
  {
    let val = false;

    if(this.likelist.includes(id))
    {
      val = true;
      console.log(val);
    }

    return val;
  }

  sendNumPage(libro, identidad)
  {
    if (this.recordpage.librofinalizado == false)
    {
        this.recordpage.user_id = identidad;
        this.recordpage.book_id = libro;

        this._bookService.recordNumPages(this.token, this.recordpage).subscribe(
          response => 
          {
            if(response.status == 'success')
            {
                console.log(response);  
                this.getNumPages(this.token, this.identity.sub, this.id);
            }
            else
            {
              console.log(response.error)
            }
            
          },
          error => {
            console.log(<any>error);
          }
        )
    }
    else
    {
        this.recordpage.user_id = identidad;
        this.recordpage.book_id = libro;

        this._bookService.deleteNumPages(this.token, this.recordpage).subscribe(
          response => 
          {
            if(response.status == 'success')
            {
                console.log(response);  
                this.getNumPages(this.token, this.identity.sub, this.id);
                this.recordpage.librofinalizado = false
            }
            else
            {
              console.log(response.error)
            }
            
          },
          error => {
            console.log(<any>error);
          }
        )
    }
  }

  getNumPages(token, identidad, libro)
  {
    this._bookService.getNumPages(token, identidad, libro).subscribe(
      response => 
      {
        if(response.status == 'success' && response.recordPage != 0)
        {
            console.log(response);  
            this.numpage = response.recordPage.numpages;
            this.recordpage.numpages = this.numpage;
        }
        else
        {
            this.numpage = null;
            this.recordpage.numpages = null;
        }
        
      },
      error => {
        console.log(<any>error);
      }
     )
  }

}
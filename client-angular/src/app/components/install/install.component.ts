import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {UserService} from '../../services/user.service';
import {InstallService} from '../../services/install.service';

@Component({
  selector: 'app-install',
  templateUrl: './install.component.html',
  providers: [UserService, InstallService],
  styleUrls: ['./install.component.css', '../../alert.component.css']
})
export class InstallComponent implements OnInit {

  public servidor;
  public usuario;
  public password;
  public bbdd;
  public status: string;
  public actual;

  public token;
  public identity;
  public install;

  constructor(private _userService: UserService, private _installService: InstallService, private _router: Router) 
  {
    this.servidor = 'localhost';
    this.usuario = 'root';
    this.password = '';
    this.bbdd = '';

    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.install = "install";

    if(this.identity != null)
    {
      this._router.navigate(['home']);
    }
  }

  ngOnInit() {

    this.getbbdd();
    this.install = "install";
  }

  getbbdd()
  {
    this._installService.getbbdd().subscribe(

			response => {
        if(response.status == 'success')
        {
          this.actual = response.bbdd;
        }
			},
			error => {
        console.log(<any>error);
			}
		);
  }

  onSubmit(form)
	{
    this.status = 'warning';

    console.log(this.servidor);
    console.log(this.usuario);
    console.log(this.password);
    console.log(this.bbdd);

    this._installService.setbbdd(this.bbdd).subscribe(

			response => {
        if(response.status == 'success')
        {
          this._router.navigate(['welcome']);
          this.usuario = '';
          this.password = '';
          this.bbdd = '';
        }
			},
			error => {
        console.log(<any>error);
        this.status = 'error';
			}
		);

	}

}

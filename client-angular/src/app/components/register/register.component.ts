import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { UserService } from '../../services/user.service';
import {User} from '../../models/user';

@Component({
	
	selector: 'register',
	templateUrl: './register.component.html',
	providers: [UserService],
	styleUrls: ['./register.component.css', '../../alert.component.css', '../../spanalert.component.css']
})

export class RegisterComponent implements OnInit
{
	public title: string;
	public user: User;
	public status: string;
	public url: any;
	public errors;
	public data;
	public divs;
	public nameimage;

	constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService)
	{
		this.title = 'CREATE UNA CUENTA';
		this.user = new User(1, '', '', '', '', '', '', 'ROLE_USER');

		//El sexto es la imagen
	}

	ngOnInit()
	{
		console.log("Registro Cargado");
	}

	onSubmit(form)
	{

		this._userService.register(this.user).subscribe(

			response => {
				if(response.status == 'success')
				{
					this.status = response.status;

					this.user = new User(1, '', '', '', '', '', '', 'ROLE_USER');
					form.reset();
				}
				else
				{
					this.status = 'erroruser';
					this.data = {'message': response.message};
					console.log(this.data);
				}
			},
			error => {
				this.status = 'error';
				this.errors = error.error;
				console.log(this.errors);
			}
		);
	}

	readUrl(event:any) 
	{
		  if (event.target.files && event.target.files[0]) {
		    var reader = new FileReader();

		    reader.onload = (event: ProgressEvent) => {
		      this.user.image = this.url = (<FileReader>event.target).result;
		    }

		    reader.readAsDataURL(event.target.files[0]);
			 (event.target.files[0]);
			 this.nameimage = event.target.files[0].name;
		  }
	}
}
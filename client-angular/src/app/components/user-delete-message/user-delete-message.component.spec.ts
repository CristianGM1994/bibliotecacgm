import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDeleteMessageComponent } from './user-delete-message.component';

describe('UserDeleteMessageComponent', () => {
  let component: UserDeleteMessageComponent;
  let fixture: ComponentFixture<UserDeleteMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDeleteMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDeleteMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

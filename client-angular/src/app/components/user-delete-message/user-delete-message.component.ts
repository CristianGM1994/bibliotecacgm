import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { Message } from '../../models/message';
import { UserService } from '../../services/user.service';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-user-delete-message',
  templateUrl: './user-delete-message.component.html',
  styleUrls: ['./user-delete-message.component.css'],
  providers: [UserService, MessageService]
})
export class UserDeleteMessageComponent implements OnInit {

  public title: string;
  public identity;
  public token;
  public id;
  public idmessage;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _messageService: MessageService) 
  {
  		this.title = 'Bandeja de Entrada';
  		this.identity = this._userService.getIdentity();
      	this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
  		this.getParams();

  		if(this.identity == null)
	    {
	        this._router.navigate(['']);
	    }
	    if(this.id != this.identity.sub)
	    {
	    	this._router.navigate(['home', { 'message': '¡No tienes permiso para poder ejecutar esta acción!', 'type': "danger" }]);
	    }

  }

  getParams()
  {
  		this._route.params.subscribe(params => {
  			this.id = params['id'];
        this.idmessage = params['idmessage'];
  		});
  }

  destroy()
  {
    console.log(this.idmessage);
    this._messageService.delete(this.token, this.idmessage).subscribe(
          response => {
            if (response.status == 'success')
            {
                console.log(response);
                this._router.navigate(['home', { 'message': "¡Mensaje eliminado!", 'type': "success" }]);
            }
            else
            {
              this._router.navigate(['home', { 'message': "El mensaje no se ha podido borrar", 'type': "danger" }]);
            }
          },
          error => {
            this._router.navigate(['home', { 'message': "El mensaje no se ha podido borrar", 'type': "danger" }]);
          }
        );
  }

}

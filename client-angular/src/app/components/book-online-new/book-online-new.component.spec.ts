import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookOnlineNewComponent } from './book-online-new.component';

describe('BookOnlineNewComponent', () => {
  let component: BookOnlineNewComponent;
  let fixture: ComponentFixture<BookOnlineNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookOnlineNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookOnlineNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

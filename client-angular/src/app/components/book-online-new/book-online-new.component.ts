import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { BookOnlineService } from '../../services/bookonline.service';
import { GenreService } from '../../services/genre.service';
import { BookOnline } from '../../models/bookonline';
import { Genre } from '../../models/genre';

@Component({
  selector: 'app-book-online-new',
  templateUrl: './book-online-new.component.html',
  styleUrls: ['./book-online-new.component.css', '../../alert.component.css', '../../spanalert.component.css'],
  providers: [UserService, BookOnlineService, GenreService]
})
export class BookOnlineNewComponent implements OnInit {

  public page_title: string;
  public identity;
  public token;
  public book: BookOnline;
  public genres: Array<Genre>;
  public nameimage;
  public url;
  public status: string;
  public errors;
  public message: string;
  public idbook;
  public edit: boolean;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _bookonlineService: BookOnlineService, private _genreService: GenreService) 
  {
      this.page_title = 'Crear Nuevo Libro Online';
      this.identity = this._userService.getIdentity();
      this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
    if(this.identity == null)
    {
      this._router.navigate(['']);
    }

      this._route.params.subscribe(data => 
      {
          this.idbook = data['id'];

          this._bookonlineService.show(this.idbook).subscribe(
            response => 
            {
              if(response.status == 'success')
              {
                  this.book = response.books;
                  this.edit = true;
                  if(this.book.user_id != this.identity.sub)
                  {
                    this._router.navigate(['home', { 'message': "No tienes permiso para visualizar esta sección", 'type': "danger" }]);
                  }
                  console.log(this.book);
              }
              else
              {
                this.book = new BookOnline(1, '', '', '', '', '', 1, 1);
                this.edit = false;
                console.log(response);
              }      
            },
            error => {
              this.book = new BookOnline(1, '', '', '', '', '', 1, 1);
              this.edit = false;
              console.log(error);
            }
        )

      })

        this.getGenre();
        this.book.user_id = this.identity.sub;
  }

  getGenre()
  {
    this._genreService.getGenre().subscribe(
      response => {
        if (response.status == 'success')
        {
          this.genres = response.genre;
          console.log(this.genres);
        }
      },
      error => {

        console.log(error);
      }
    );
  }

  readUrl(event:any, archivo) 
  {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
         this.url = (<FileReader>event.target).result;
         this.book.bookimage = this.url;
        }

        reader.readAsDataURL(event.target.files[0]);
         (event.target.files[0]);
         this.nameimage = event.target.files[0].name;
      }
  }

  onSubmit(form)
  {

    if(this.edit == false)
    {
            this._bookonlineService.create(this.token, this.book).subscribe(
            response => 
            {
              if(response.status == 'success')
              {
        
                  this.message = '¡Libro Online Creado!';

                  this._router.navigate(['../bookonlinetable', { 'message': this.message, 'type': "success" }]);

              }
              else
              {
                this.status = 'error';
                this.errors = response.error;
              }
              
            },
            error => {
              console.log(<any>error);
              this.status = 'error';
              this.errors = error.error;
            }
          )
    }
    else
    {
          this._bookonlineService.update(this.token, this.book, this.book.id).subscribe(
            response => 
            {
              if(response.status == 'success')
              {
        
                  this.message = '¡Libro Online Modificado!';

                  this._router.navigate(['../bookonlinetable', { 'message': this.message, 'type': "success" }]);

              }
              else
              {
                this.status = 'error';
                this.errors = response.error;
              }
              
            },
            error => {
              console.log(<any>error);
              this.status = 'error';
              this.errors = error.error;
            }
          )
    }
  }

}

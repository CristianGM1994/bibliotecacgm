import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookOnlineWriteComponent } from './book-online-write.component';

describe('BookOnlineWriteComponent', () => {
  let component: BookOnlineWriteComponent;
  let fixture: ComponentFixture<BookOnlineWriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookOnlineWriteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookOnlineWriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

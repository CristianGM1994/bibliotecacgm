import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { BookService } from '../../services/book.service';

@Component({
  selector: 'app-find-book',
  templateUrl: './find-book.component.html',
  styleUrls: ['./find-book.component.css'],
  providers: [BookService]
})
export class FindBookComponent implements OnInit {

  public list;
  public identity;
  public token;
  tipo: string;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _bookService: BookService) 
  {
  	this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
  	  if(this.identity == null)
      {
        this._router.navigate(['']);
      }
      this.tipo = "titulo";
  }

  getConsulta(tipo, nombre)
  {
    this._bookService.consultarlibro(tipo,nombre).subscribe(
      response => {
          this.list = response['book'];
          console.log(response);
      },
      error => {

        console.log(error);
      }
    );
  }

}

import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { UserService } from '../../services/user.service';
import {User} from '../../models/user';
import {Log} from '../../models/log';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  providers: [UserService],
  styleUrls: ['./log.component.css']
})
export class LogComponent implements OnInit {

  public title: string;
  public user: User;
  public token;
  public identity;
  public listalog: Array<Log>;
  public p: number = 1;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService) 
  { 
      this.title = 'Sección del Admin';
		  this.identity = this._userService.getIdentity();
  		this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
    if(this.identity == null || this.identity.role != 'ROLE_ADMIN')
    {
        this._router.navigate(['home', { 'message': "No tienes permiso para visualizar esta sección", 'type': "danger" }]);
    }
      
      this.getLogs(this.token);
  }

  getLogs(token)
  {
    this._userService.getLogs(token).subscribe(
      response => {
        this.listalog = response.listado;
        console.log(this.listalog);
      },
      error => {

        console.log(error);
      }
    );
  }

}

import {Component} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { UserService } from '../../services/user.service';
import {User} from '../../models/user';

@Component({
	selector: 'welcome',
	templateUrl: './welcome.component.html',
	providers: [UserService],
	styleUrls: ['./welcome.component.css']
})

export class WelcomeComponent
{
	
}
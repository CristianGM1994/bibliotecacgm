import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { UserService } from '../../services/user.service';
import {User} from '../../models/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  providers: [UserService],
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  public title: string;
  public user: User;
  public token;
  public identity;
	public listausers: Array<User>;
	public p: number = 1;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService) 
  {
  	this.title = 'Listado de Usuarios';
	this.identity = this._userService.getIdentity();
  	this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
  	if(this.identity == null || this.identity.role != 'ROLE_ADMIN')
	{
	        this._router.navigate(['home', { 'message': "No tienes permiso para visualizar esta sección", 'type': "danger" }]);
	}
	this.getUsers();
  }

  getUsers()
  {
	    this._userService.getUsers().subscribe(
	      response => {
	        if (response.status == 'success')
	        {
	          this.listausers = response.user;
	          console.log(this.listausers);
	        }
	      },
	      error => {

	        console.log(error);
	      }
	    );
	}
	
	generateAdmin(id)
	{
		this._userService.getadmin(this.token, id).subscribe(
			response => {
				if (response.status == 'success')
				{
					
					console.log(response);
					this.getUsers();
				}
			},
			error => {

				console.log(error);
			}
		);
	}

	removeAdmin(id)
	{
		this._userService.removeadmin(this.token, id).subscribe(
			response => {
				if (response.status == 'success')
				{
					
					console.log(response);
					this.getUsers();
				}
			},
			error => {

				console.log(error);
			}
		);
	}

}

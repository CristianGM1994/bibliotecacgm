import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {User} from '../../models/user';
import { UserService } from '../../services/user.service';
import { Genre } from '../../models/Genre';
import { GenreService } from '../../services/genre.service';

@Component({
  selector: 'app-add-genre',
  templateUrl: './add-genre.component.html',
  providers: [UserService, GenreService],
  styleUrls: ['./add-genre.component.css', '../../alert.component.css']
})
export class AddGenreComponent implements OnInit {

  public title: string;
  public user: User;
  public token;
  public identity;
  public genre: Genre;
  public status: string;
  public errors;
  public error;
  public message;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _genreService: GenreService) 
  {
  	  this.title = 'Agregar Genero';
	  this.identity = this._userService.getIdentity();
  	  this.token = this._userService.getToken();
  	  this.genre = new Genre(1, '');
  }

  ngOnInit() 
  {
		if(this.identity == null || this.identity.role != 'ROLE_ADMIN')
		{
		    this._router.navigate(['home', { 'message': "No tienes permiso para visualizar esta sección", 'type': "danger" }]);
		}
  }

   onSubmit(form)
  {
  		console.log(this.genre);
	    this._genreService.create(this.token, this.genre).subscribe(
	       response => 
	      {
	        if(response.status == 'success')
	        {  	
	        	this.status = 'success';
	        	this.message = response.message;
	        }
	        else
	        {
	          this.status = 'error';
	          this.error = response.error;
	          console.log(response);
	        }
	        
	      },
	      error => 
	      {
		        console.log(<any>error);
		        this.status = 'error';
		        this.errors = error.error;
	      }
	     )
  }

}

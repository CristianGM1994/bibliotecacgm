import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.css'],
  providers: [UserService]
})
export class UserDeleteComponent implements OnInit {

  public identity;
  public token;
  public id;
  public user: User;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService) 
  {
  	 this.identity = this._userService.getIdentity();
     this.token = this._userService.getToken();
     this.user = new User(1, '', '', '', '', '', '', 'ROLE_USER');
  }

  ngOnInit() 
  {
  		if(this.identity == null)
	    {
	        this._router.navigate(['']);
	    }
	    if(this.identity.role != "ROLE_ADMIN")
	    {
	    	this._router.navigate(['home', { 'message': '¡No tienes permiso para poder ejecutar esta acción!', 'type': "danger" }]);
	    }

	    this.getParams();
  }

  getParams()
  {
  		this._route.params.subscribe(params => {
  			this.id = params['id'];
  			console.log(this.id);
  		});
  }

  destroy()
  {
    	this._userService.destroy(this.token, this.id).subscribe(
        response => {
          if(response.status == 'success')
          {
            this._router.navigate(['admin', { 'message': '¡Se eliminó al usuario!', 'type': "success" }]);
          }
          else
          {
            console.log(response);
          }
        },
        error => {
          console.log(<any>error);
        }
      );
  }

}

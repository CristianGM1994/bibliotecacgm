import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import * as $ from 'jquery';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css', '../../alert.component.css', '../../spanalert.component.css'],
  providers: [UserService]
})
export class UserEditComponent implements OnInit {

  public identity;
  public token;
  public status: string;
  public page_title: string;
  public user: User;
  public password_confirmation: string;
  public errors;
  public data;
  public url: any;
  public id: number;
  public nameimage;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService) 
  {
    this.page_title = 'Edición de Usuario';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();  
    this.user = new User(1, '', '', '', '', '', '', 'ROLE_USER'); 
  }

  ngOnInit() 
  {     
      this.getUser();
      if(this.identity != null && this.identity.sub == this.id || this.identity.role == 'ROLE_ADMIN')
      {
        $( document ).ready(function() 
        {
            $('#cpassword').hide();
            $("#changepassword").click(function() {
              $('#cpassword').toggle();
            });
        });
      }
      else
      {
        this._router.navigate(['home', { 'message': "No tienes permiso para editar este perfil", 'type': "danger" }]);
      }
  }

  getUser()
  {
      this._route.params.subscribe(params => {
        this.id = +params['id'];

        this._userService.getUser(this.id).subscribe(
          response => {
            if (response.status == 'success')
            {
              this.user = response.user;
              console.log(this.user);
            }
            else
            {
              this._router.navigate(['home', { 'message': "No se ha podido ejecutar la busqueda" }]);
            }
          },
          error => {
            this._router.navigate(['home', { 'message': "No se ha podido ejecutar la busqueda", 'type': "danger" }]);
          }
        );
      });
  }

  readUrl(event:any) 
  {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
           this.url = (<FileReader>event.target).result;
           this.user.image = this.url;
        }

        reader.readAsDataURL(event.target.files[0]);
         (event.target.files[0]);
         this.nameimage = event.target.files[0].name;
      }
  }

  onSubmit(form)
  {
    console.log(this.user);
    this._userService.update(this.token, this.user, this.user.id).subscribe(
        response => {
          if(response.status == 'success')
          {
            this.status = response.status;  
            if(response.user.sub == this.identity.sub)
            {
                localStorage.setItem('identity', JSON.stringify(response.user));
            }
            this.user = response.user;
            this.getUser();
          }
          else
          {
            this.status = 'error';
            console.log("Error");
            this.getUser();
          }
        },
        error => {           
            this.status = 'error';
            this.errors = error.error;
            console.log(this.errors);
            this.getUser();
        }
      );

      this.getUser();
  }
}

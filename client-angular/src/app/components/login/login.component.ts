import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {User} from '../../models/user';
import {UserService} from '../../services/user.service';

@Component({
	
	selector: 'login',
	templateUrl: './login.component.html',
	providers: [UserService],
	styleUrls: ['./login.component.css', '../../alert.component.css', '../../spanalert.component.css']
})

export class LoginComponent implements OnInit
{
	public title: string;
	public user: User;
	public status: string;
	public error;
	public token;
	public identity;

	constructor(private _userService: UserService, private _route: ActivatedRoute, private _router: Router)
	{
		this.title = 'Identificate';
		this.user = new User(1, '', '', '', '', '', '', 'ROLE_USER');
	}

	ngOnInit()
	{
		if(this.identity != null)
		{
			this._router.navigate(['home']);
		}
		this.logout();
	}

	onSubmit(form)
	{
		console.log(this.user);

		this._userService.signup(this.user).subscribe
		(
				response => 
				{
					if(response.status != 'Error')
					{
						//Token
						console.log(response);
						this.token = response;
						localStorage.setItem('token', this.token);

						this._userService.signup(this.user, true).subscribe(
							response => 
							{
								//OBJETO USUARIO IDENTIFICADO
								console.log(response);
								this.identity = response;
								localStorage.setItem('identity', JSON.stringify(this.identity));
								this._router.navigate(['home']);

							},
							error => 
							{
								console.log(<any>error);
								this.status = 'error';
							}
					    );
				    }
				    else
				    {
				    	this.status = 'error';
				    	this.error = response;
				    }

				},
				error =>
				{
					console.log(<any>error);
					this.status = 'error';
				},
		);
	}

	logout()
	{
		this._route.params.subscribe(params =>
		{	
			let logout = +params['sure'];

			if(logout == 1)
			{
				localStorage.removeItem('identity');
				localStorage.removeItem('token');

				this.identity = null;
				this.token = null;

				//Redirección a otro componente
				this._router.navigate(['login']);
			}
		});
	}
}
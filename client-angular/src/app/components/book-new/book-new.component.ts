import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { BookService } from '../../services/book.service';
import { GenreService } from '../../services/genre.service';
import { Book } from '../../models/book';
import { Genre } from '../../models/genre';

@Component({
  selector: 'app-book-new',
  templateUrl: './book-new.component.html',
  styleUrls: ['./book-new.component.css', '../../alert.component.css', '../../spanalert.component.css'],
  providers: [UserService, BookService, GenreService]
})
export class BookNewComponent implements OnInit {

  public page_title: string;
  public identity;
  public token;
  public book: Book;
  public genres: Array<Genre>;
  public status: string;
  public errors;
  public message: string;
  public url;
  public aceptar: boolean;
  public nameimage;
  public namelibro;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _genreService: GenreService, private _bookService: BookService) 
  {
  	  this.page_title = 'Subir Nuevo Libro';
      this.identity = this._userService.getIdentity();
      this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
      if(this.identity == null)
      {
        this._router.navigate(['']);
      }
      else
      {
        this.book = new Book(1, '', '', '', '', '', 1, 1);
        this.aceptar = false;
      }

        this.getGenre();
        this.book.user_id = this.identity.sub;
  }

  mandaraceptar()
  {
    if(this.aceptar == false)
    {
      this.aceptar = true;
    }
    else
    {
      this.aceptar = false;
    }
  }

  getGenre()
  {
    this._genreService.getGenre().subscribe(
      response => {
        if (response.status == 'success')
        {
          this.genres = response.genre;
          console.log(this.genres);
        }
      },
      error => {

        console.log(error);
      }
    );
  }

  readUrl(event:any, archivo) 
  {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
         this.url = (<FileReader>event.target).result;
         this.book.bookimage = this.url;
        }

        reader.readAsDataURL(event.target.files[0]);
         (event.target.files[0]);
         this.nameimage = event.target.files[0].name;
      }
  }

  readPDF(event:any, archivo) 
  {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();

        reader.onload = (event: ProgressEvent) => {
           this.url = (<FileReader>event.target).result;
           this.book.bookurl = this.url;
        }

        reader.readAsDataURL(event.target.files[0]);
         (event.target.files[0]);
         this.namelibro = event.target.files[0].name;
      }
  }

  onSubmit(form)
  {
    this._bookService.create(this.token, this.book).subscribe(
        response => 
      {
        if(response.status == 'success')
        {
          if(this.identity.role == 'ROLE_ADMIN')
          {
            this.message = '¡Libro creado con exito'!;
          }
          else
          {
            this.message = '¡El libro se mandó a revisar. Un Administrador decidirá si el contenido es adecuado y te avisará sobre la decisión por mensaje privado!';
          }
          this._router.navigate(['home', { 'message': this.message, 'type': "success" }]);

        }
        else
        {
          this.status = 'error';
          this.errors = response.error;
        }
        
      },
      error => {
        console.log(<any>error);
        this.status = 'error';
        this.errors = error.error;
      }
     )
  }

}

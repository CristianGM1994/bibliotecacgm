import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { BookService } from '../../services/book.service';
import { Book } from '../../models/book';
import { LikeService } from '../../services/like.service';
import { Like } from '../../models/like';
import { FilterPipe }from '../../pipes/filter.pipe';
import { NgModule }      from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import * as $ from 'jquery';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css'],
  providers: [UserService, BookService, LikeService]
})
export class BookListComponent implements OnInit {

  public page_title: string;
  public identity;
  public token;
  public list: Array<Book>;
  public list2: Array<Book>;
  public status: string;
  public message: string;
  public filtername: string;
  public likelist;
  public p: number = 1;
  tipo: string;
  orden: string;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _bookService: BookService, private _likeService: LikeService) 
  {
  	this.page_title = 'Listado de Libros';
  	this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
  	  if(this.identity == null)
      {
        this._router.navigate(['']);
      }

      this.getBooks();

      this.getLikes(this.identity.sub);

      this.tipo = "id";
      this.orden = "asc";
      this.sortBy("id", "asc");

  }

  getBooks()
  {
    this._bookService.getBooks().subscribe(
      response => {
        if (response.status == 'success')
        {
          this.list = response.books;
          this.list2 = response.books;
        }
      },
      error => {

        console.log(error);
      }
    );
  }

  sortBy(tipo, orden) 
  {
    this._bookService.orderbook(tipo, orden).subscribe(
      response => {
        if (response.status == 'success')
        {
          this.list = response.books;
          this.list2 = response.books;
          console.log(this.list2);
        }
      },
      error => {

        console.log(error);
      }
    );

    this.list = this.list2;
      return this.list;
  }

  sendLike(id)
  {
    this._likeService.create(this.token, this.identity.sub, id).subscribe(
      response => 
      {
        if(response.status == 'success')
        {
            console.log(response);
            this.getLikes(this.identity.sub);   
        }
        else
        {
          console.log(response.error)
        }
        
      },
      error => {
        console.log(<any>error);
      }
     )

     this.getBooks();
  }

  sendDislike(id)
  {
    this._likeService.destroy(this.token, this.identity.sub, id).subscribe(
      response => 
      {
        if(response.status == 'success')
        {
          console.log(response);
          console.log("Destruido");
          this.getLikes(this.identity.sub);  
        }
        else
        {
          console.log(response.error)
        }
        
      },
      error => {
        console.log(<any>error);
      }
     )
  }

  getLikes(id)
  {
    this._likeService.getLikes(this.token, id).subscribe(
      response => 
      {
        if(response.status == 'success')
        {
          this.likelist = response.likes;
          console.log(this.likelist);

        }
        else
        {
          console.log(response.error)
        }
        
      },
      error => {
        console.log(<any>error);
      }
     )
  }

  checkLike(id)
  {
    let val = false;

    if(this.likelist.includes(id))
    {
      val = true;
      console.log(val);
    }

    return val;
  }

}

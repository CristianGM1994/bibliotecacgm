import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { Message } from '../../models/message';
import { UserService } from '../../services/user.service';
import { MessageService } from '../../services/message.service';


@Component({
  selector: 'app-user-send-message',
  templateUrl: './user-send-message.component.html',
  styleUrls: ['./user-send-message.component.css', '../../alert.component.css', '../../spanalert.component.css'],
  providers: [UserService, MessageService],
})
export class UserSendMessageComponent implements OnInit {

  public title: string;
  public user: User;
  public message: Message;
  public recibido: string;
  public identity;
  public token;
  public status: string;
  public errors;
  public error;
  public userlist: Array<User>;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _messageService: MessageService) 
  {
  		this.title = 'Enviar Un Mensaje';
  		this.identity = this._userService.getIdentity();
      	this.token = this._userService.getToken();

		this.user = new User(1, '', '', '', '', '', '', 'ROLE_USER');
		this.message = new Message(1, '', '', 1, 1);		
  }

  ngOnInit() 
  {
  		if(this.identity == null)
	    {
	        this._router.navigate(['']);
	    }

	    this.getUser();
	    this.message.user_send = this.identity.sub;
  }

  getUser()
  {
	  	this._route.params.subscribe(params => {
	  		console.log(params);
	  		let id = +params['id'];
	  		if(params['message'])
	  		{
		  		this.recibido = params['message'];
		  		console.log(this.recibido);
	  		}

	  		this._userService.getUser(id).subscribe(
	  			response => {
	  				if (response.status == 'success')
	  				{
	  					this.user = response.user;
	  					this.message.user_receive = this.user.id;
	  				}
	  				else
	  				{
	  					this.user = null;
	  					this.getUsers();
	  				}
	  			},
	  			error => {
	  				this.user = null;
	  				this.getUsers();
	  			}
	  		);
	  	});
	}

	getUsers()
	{
		this._userService.getUsers().subscribe(
	  			response => {
	  				if (response.status == 'success')
	  				{
	  					this.userlist = response.user;
	  					console.log(response);
	  				}
	  				else
	  				{
	  					console.log(response);
	  				}
	  			},
	  			error => {
	  				console.log(error);
	  			}
	  		);
	}

  onSubmit(form)
  {
    this._messageService.create(this.token, this.message).subscribe(
      response => 
      {
        if(response.status == 'success')
        {

          this._router.navigate(['home', { 'message': "Mensaje enviado con exito", 'type': "success" }]);

        }
        else
        {
          this.status = 'error';
          this.errors = response;
          console.log(this.errors);
        }
        
      },
      error => {
        console.log(<any>error);
        this.status = 'error';
        this.errors = error.error;
      }
     )
  }

}

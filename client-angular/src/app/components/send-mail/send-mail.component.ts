import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { BookService } from '../../services/book.service';
import { Book } from '../../models/book';
import { Mail } from '../../models/mail';

@Component({
  selector: 'app-send-mail',
  templateUrl: './send-mail.component.html',
  styleUrls: ['./send-mail.component.css', '../../spanalert.component.css',  '../../alert.component.css'],
  providers: [UserService, BookService]
})
export class SendMailComponent implements OnInit {

  public page_title: string;
  public identity;
  public token;
  public book: Book;
  public id;
  public email;
  public mensaje;
  public sendmail: Mail;
  public status: string;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _bookService: BookService) 
  {
      this.page_title = 'Enviar Un Libro';
  	  this.identity = this._userService.getIdentity();
      this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
      if(this.identity == null)
      {
          this._router.navigate(['']);
      }
      else
      {
          this.getBook();
          this.sendmail = new Mail(this.id, '', '');
      }
      
  }

  getBook()
  {
	  	this._route.params.subscribe(params => {
	  		this.id = +params['id'];

	  		this._bookService.getBook(this.id).subscribe(
	  			response => {
	  				if (response.status == 'success')
	  				{
	  					this.book = response.books;
	  				}
	  				else
	  				{
	  					this._router.navigate(['home', { 'message': "No se ha podido ejecutar la busqueda", 'type': "danger" }]);
	  				}
	  			},
	  			error => {
	  				this._router.navigate(['home', { 'message': "No se ha podido ejecutar la busqueda", 'type': "danger" }]);
	  			}
	  		);
	  	});
  }
  
  onSubmit(form)
  {

    this.status = 'warning';

    this._bookService.mail(this.token, this.sendmail).subscribe(

			response => {
				if(response.status == 'success')
				{
					this._router.navigate(['home', { 'message': "¡Mensaje Enviado Con Exito!", 'type': "success" }]);
				}
				else
				{
					this._router.navigate(['home', { 'message': "¡No se pudó enviar! Surgió un problema, intentalo mas tarde", 'type': "danger" }]);
				}
			},
			error => {
				this._router.navigate(['home', { 'message': "¡No se pudó enviar! Surgió un problema, intentalo mas tarde", 'type': "danger" }]);
			}
		);
  }

}

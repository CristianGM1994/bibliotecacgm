import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { Book } from '../../models/book';
import { BookService } from '../../services/book.service';
import { Message } from '../../models/message';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-book-delete',
  templateUrl: './book-delete.component.html',
  styleUrls: ['./book-delete.component.css'],
  providers: [UserService, BookService, MessageService]
})
export class BookDeleteComponent implements OnInit {

  public identity;
  public token;
  public id;
  public message: Message;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _messageService: MessageService, private _bookService: BookService) 
  {
  	this.identity = this._userService.getIdentity();
	this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
  	if(this.identity == null || this.identity.role != 'ROLE_ADMIN')
    {
        this._router.navigate(['home', { 'message': "No tienes permiso para visualizar esta sección", 'type': "danger" }]);
    }

    this.getParams();
  }

  getParams()
  {
	this._route.params.subscribe(params => {
		this.id = params['id'];
	});
  }

  destroy()
  {
    this._bookService.destroy(this.token, this.id).subscribe(
          response => {
            if (response.status == 'success')
            {
                this.message = new Message(1, 'Novedades sobre su publicación', 'Le enviamos este mensaje para informarle de la eliminación de su obra de nuestra plataforma. Para más información, responda a este mensaje.', response.book.user_id, this.identity.sub);

                this._messageService.create(this.token, this.message).subscribe(
    			      response => 
    			      {
    			        if(response.status == 'success')
    			        {
    	                	this._router.navigate(['home', { 'message': "¡Libro eliminado con exito!", 'type': "success" }]);
    	                }
    	                else
    			        {
    			          console.log("Ocurrió un problema");
    			        }		        
    			      },
    			      error => {
    			        console.log(<any>error);
    			      }
    			     )
                }
                else
                {
                  console.log(response);
                }
          },
          error => {
            console.log(error);
          }
        );
  }

}

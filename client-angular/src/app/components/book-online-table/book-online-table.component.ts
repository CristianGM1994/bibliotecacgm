import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { BookOnlineService } from '../../services/bookonline.service';
import { GenreService } from '../../services/genre.service';
import { BookOnline } from '../../models/bookonline';
import { Genre } from '../../models/genre';

@Component({
  selector: 'app-book-online-table',
  templateUrl: './book-online-table.component.html',
  styleUrls: ['./book-online-table.component.css', '../../alert.component.css', '../../spanalert.component.css'],
  providers: [UserService, BookOnlineService, GenreService]
})
export class BookOnlineTableComponent implements OnInit {

  public page_title: string;
  public identity;
  public token;
  public booksonline: Array<BookOnline>;
  public p: number = 1;
  
  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _bookonlineService: BookOnlineService, private _genreService: GenreService) 
  {
      this.page_title = 'Crear Nuevo Libro Online';
      this.identity = this._userService.getIdentity();
      this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
      if(this.identity == null)
      {
        this._router.navigate(['']);
      }

        this.getBooksOnline();
  }

  getBooksOnline()
  {
    this._bookonlineService.search(this.identity.sub).subscribe(
      response => 
      {
        if(response.status == 'success')
        {
            this.booksonline = response.books;
            console.log(this.booksonline);
        }
        else
        {
          console.log(response);
        }
        
      },
      error => {
        console.log(<any>error);
      }
    )
  }

  sendrevision(bookonline)
  {
    this._bookonlineService.sendrevision(this.token, bookonline).subscribe(
      response => 
      {
        if(response.status == 'success')
        {
            this.getBooksOnline();
        }
        else
        {
          console.log(response);
        }
        
      },
      error => {
        console.log(<any>error);
      }
    )
  }


}

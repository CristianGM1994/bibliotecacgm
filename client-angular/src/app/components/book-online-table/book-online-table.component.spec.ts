import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookOnlineTableComponent } from './book-online-table.component';

describe('BookOnlineTableComponent', () => {
  let component: BookOnlineTableComponent;
  let fixture: ComponentFixture<BookOnlineTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookOnlineTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookOnlineTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

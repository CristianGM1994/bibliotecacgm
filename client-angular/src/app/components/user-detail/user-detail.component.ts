import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { Book } from '../../models/book';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  public user: User;
  public title: string;
  public token;
  public identity;
  public id: number;
  public list: Array<Book>;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService) 
  {
  		this.title = 'Detalle de ';
		this.identity = this._userService.getIdentity();
  		this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
  		if(this.identity == null)
	    {
	        this._router.navigate(['']);
	    }

	    this.getUser();
  }

  getUser()
  {
      this._route.params.subscribe(params => {
        this.id = +params['id'];

        this._userService.getUser(this.id).subscribe(
          response => {
            if (response.status == 'success')
            {
              this.user = response.user;
              this.list = response.user.book;
              console.log(response.user.book);
            }
            else
            {
              this._router.navigate(['home', { 'message': "No se ha podido ejecutar la busqueda", 'type': "danger" }]);
            }
          },
          error => {
            this._router.navigate(['home', { 'message': "No se ha podido ejecutar la busqueda", 'type': "danger" }]);
          }
        );
      });
  }

}

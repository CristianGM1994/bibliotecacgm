import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Book } from '../../models/book';
import { User } from '../../models/user';
import { Like } from '../../models/like';
import { UserService } from '../../services/user.service';
import { BookService } from '../../services/book.service';
import { LikeService } from '../../services/like.service';

@Component({
  selector: 'app-user-like',
  templateUrl: './user-like.component.html',
  styleUrls: ['./user-like.component.css'],
  providers: [UserService, BookService, LikeService]
})
export class UserLikeComponent implements OnInit {

	public user: User;
	public id: number;
	public page_title: string;
	public identity;
	public token;
  public likelist: Array<Like>;
  public likeusers;
  public p: number = 1;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _bookService: BookService, private _likeService: LikeService) 
  {
  	  this.page_title = 'Listado de Favoritos';
  	  this.identity = this._userService.getIdentity();
      this.token = this._userService.getToken();
      this.user = new User(1, '', '', '', '', '', '', 'ROLE_USER');
  }

  ngOnInit() 
  {
  	if(this.identity == null)
    {
        this._router.navigate(['']);
    }

    this.getUser();
    this.showLikes(this.id);
  }

  getUser()
  {
      this._route.params.subscribe(params => {
        this.id = +params['id'];

        this._userService.getUser(this.id).subscribe(
          response => {
            if (response.status == 'success')
            {
              this.user = response.user;
            }
            else
            {
              this._router.navigate(['home', { 'message': "No se ha podido ejecutar la busqueda", 'type': "danger" }]);
            }
          },
          error => {
            this._router.navigate(['home', { 'message': "No se ha podido ejecutar la busqueda", 'type': "danger" }]);
          }
        );
      });
  }

  showLikes(id)
  {
    this._likeService.showLikes(this.token, id).subscribe(
      response => 
      {
        if(response.status == 'success')
        {
          this.likelist = response.likes;
          this.likeusers = response.likesuser;
          console.log(this.likeusers);
        }
        else
        {
          console.log(response.error)
        }
        
      },
      error => {
        console.log(<any>error);
      }
     )
  }

  sendLike(id)
  {
    this._likeService.create(this.token, this.identity.sub, id).subscribe(
      response => 
      {
        if(response.status == 'success')
        {
            console.log(response);
            this.showLikes(this.id);   
        }
        else
        {
          console.log(response.error)
        }
        
      },
      error => {
        console.log(<any>error);
      }
     )
  }

  sendDislike(id)
  {
    this._likeService.destroy(this.token, this.identity.sub, id).subscribe(
      response => 
      {
        if(response.status == 'success')
        {
          console.log(response);
          console.log("Destruido");
          this.showLikes(this.id);  
        }
        else
        {
          console.log(response.error)
        }
        
      },
      error => {
        console.log(<any>error);
      }
     )
  }

  checkLike(id)
  {
    let val = false;

    if(this.likeusers.includes(id))
    {
      val = true;
    }

    return val;
   }

}

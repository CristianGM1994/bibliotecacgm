import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserReceiveMessageComponent } from './user-receive-message.component';

describe('UserReceiveMessageComponent', () => {
  let component: UserReceiveMessageComponent;
  let fixture: ComponentFixture<UserReceiveMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserReceiveMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserReceiveMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

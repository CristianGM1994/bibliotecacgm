import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { Message } from '../../models/message';
import { UserService } from '../../services/user.service';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-user-receive-message',
  templateUrl: './user-receive-message.component.html',
  styleUrls: ['./user-receive-message.component.css'],
  providers: [UserService, MessageService]
})
export class UserReceiveMessageComponent implements OnInit {
	public title: string;
	public user: User;
	public message: Array<Message>;
	public identity;
  	public token;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _messageService: MessageService) 
  {
		this.title = 'Bandeja de Entrada';
  		this.identity = this._userService.getIdentity();
      	this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
		if(this.identity == null)
	    {
	        this._router.navigate(['']);
	    }

	    this.getMessage();
  }

  	getMessage()
	{
		this._messageService.index(this.token, this.identity.sub).subscribe(
	  			response => {
	  				if (response.status == 'success')
	  				{
	  					this.message = response.messages;
	  					console.log(this.message);
	  				}
	  				else
	  				{
	  					console.log(response);
	  				}
	  			},
	  			error => {
	  				console.log(error);
	  			}
	  		);
	}

}

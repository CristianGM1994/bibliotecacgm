import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { RevisionBook } from '../../models/revisionbook';
import { RevisionBookService } from '../../services/revisionbook.service';
import { BookService } from '../../services/book.service';
import { Message } from '../../models/message';
import { MessageService } from '../../services/message.service';
import { FilterPipe }from '../../pipes/filter.pipe';
import { NgModule }      from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

@Component({
  selector: 'app-revision-book-list',
  templateUrl: './revision-book-list.component.html',
  styleUrls: ['./revision-book-list.component.css'],
  providers: [UserService, RevisionBookService, MessageService, BookService]
})
export class RevisionBookListComponent implements OnInit {

  public page_title: string;
  public identity;
  public token;
  public list: Array<RevisionBook>;
  public message: Message;
  public p: number = 1;
  tipo: string;
  orden: string;


  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _revisionbookService: RevisionBookService, private _messageService: MessageService, private _bookService: BookService) 
  {
	this.page_title = 'Listado de Libros A Revisar';
  	this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();	
  }

  ngOnInit() 
  {
	if(this.identity == null || this.identity.role != 'ROLE_ADMIN')
    {
        this._router.navigate(['home', { 'message': "No tienes permiso para visualizar esta sección", 'type': "danger" }]);
    }
    this.getBooks();
    this.tipo = "id";
      this.orden = "asc";
  }

  getBooks()
  {
    this._revisionbookService.getBooks().subscribe(
      response => {
        if (response.status == 'success')
        {
          this.list = response.books;
          console.log(this.list);
        }
      },
      error => {

        console.log(error);
      }
    );
  }

  sortBy(tipo, orden) 
  {
    this._revisionbookService.orderbook(tipo, orden).subscribe(
      response => {
        if (response.status == 'success')
        {
          this.list = response.books;
          console.log(this.list);
        }
      },
      error => {

        console.log(error);
      }
    );

      return this.list;

  }

  update(id)
  {
    this._revisionbookService.update(this.token, id).subscribe(
      response => {
        if (response.status == 'success')
        {
          	this.message = new Message(1, 'Novedades sobre su publicación', 'Le enviamos este mensaje para informarle de la aprobación sobre la solicitud de la obra enviada. Ya está disponible para todos los usuarios. ¡Gracias por aportarla, y sigue disfrutando de nuestra plataforma!', response.book.user_id, this.identity.sub);

          	this._messageService.create(this.token, this.message).subscribe(
		      response => 
		      {
		        if(response.status == 'success')
		        {

		          this._router.navigate(['admin', { 'message': "Libro aprobado con exito", 'type': "success" }]);

		        }
		        else
		        {
		          console.log("Ocurrió un problema");
		        }		        
		      },
		      error => {
		        console.log(<any>error);
		      }
		     )

        }
      },
      error => {

        console.log(error);
      }
    );
  }

}

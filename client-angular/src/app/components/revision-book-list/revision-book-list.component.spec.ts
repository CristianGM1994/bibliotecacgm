import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisionBookListComponent } from './revision-book-list.component';

describe('RevisionBookListComponent', () => {
  let component: RevisionBookListComponent;
  let fixture: ComponentFixture<RevisionBookListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevisionBookListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisionBookListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { Message } from '../../models/message';
import { MessageService } from '../../services/message.service';
import { Comment } from '../../models/comment';
import { CommentService } from '../../services/comment.service';

@Component({
  selector: 'app-user-delete-comment',
  templateUrl: './user-delete-comment.component.html',
  styleUrls: ['./user-delete-comment.component.css'],
  providers: [UserService, MessageService, CommentService]
})
export class UserDeleteCommentComponent implements OnInit {

  public title: string;
  public identity;
  public token;
  public idcomment;
  public id;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _messageService: MessageService, private _commentService: CommentService) 
  {
    this.title = 'Bandeja de Entrada';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
      this.getParams();

      if(this.identity == null)
	    {
	        this._router.navigate(['']);
	    }
	    if(this.id != this.identity.sub && this.identity.role != 'ROLE_ADMIN')
	    {
	    	this._router.navigate(['home', { 'message': '¡No tienes permiso para poder ejecutar esta acción!', 'type': "danger" }]);
	    }
  }

  getParams()
  {
  		  this._route.params.subscribe(params => {
        this.idcomment = params['idcomment'];
        this.id = params['id'];
  		});
  }

  destroy()
  {
    this._commentService.delete(this.token, this.idcomment).subscribe(
          response => {
            if (response.status == 'success')
            {
                console.log(response);
                this._router.navigate(['home', { 'message': "¡Mensaje eliminado!", 'type': "success" }]);
            }
            else
            {
              this._router.navigate(['home', { 'message': "El mensaje no se ha podido borrar", 'type': "danger" }]);
            }
          },
          error => {
            this._router.navigate(['home', { 'message': "El mensaje no se ha podido borrar", 'type': "danger" }]);
          }
        );
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDeleteCommentComponent } from './user-delete-comment.component';

describe('UserDeleteCommentComponent', () => {
  let component: UserDeleteCommentComponent;
  let fixture: ComponentFixture<UserDeleteCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDeleteCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDeleteCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

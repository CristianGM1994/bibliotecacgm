import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSeeMessageComponent } from './user-see-message.component';

describe('UserSeeMessageComponent', () => {
  let component: UserSeeMessageComponent;
  let fixture: ComponentFixture<UserSeeMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSeeMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSeeMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { Message } from '../../models/message';
import { UserService } from '../../services/user.service';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-user-see-message',
  templateUrl: './user-see-message.component.html',
  styleUrls: ['./user-see-message.component.css'],
  providers: [UserService, MessageService],
})
export class UserSeeMessageComponent implements OnInit {
  
  public title: string;
  public identity;
  public token;
  public idmessage;
  public message: Message;

  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _messageService: MessageService) 
  { 
  	this.title = "Ver Mensaje";
	this.identity = this._userService.getIdentity();
	this.token = this._userService.getToken();
	this.message = new Message(1, '', '', 1, 1);
  }

  ngOnInit() 
  {

  	this._route.params.subscribe(data => 
    {
    	this.idmessage = data['idmessage'];
    	console.log(this.idmessage);
	})

    this.showMessage();

  	if(this.identity == null)
    {
        this._router.navigate(['']);
    }

    this.defineMessage();
  }

  	showMessage()
	{
		this._messageService.show(this.token, this.idmessage).subscribe(
  			response => {
  				if (response.status == 'success')
  				{
  					this.message = response.messages;
  					if(this.identity.sub != this.message.user_receive)
				    {
				        this._router.navigate(['home', { 'message': "No puedes visualizar este mensaje", 'type': "danger" }]);
				    }
				    console.log(this.message);
  				}
  				else
  				{
  					console.log(response);
  				}
  			},
  			error => {
  				console.log(<any>error);
  			}
  		);
	}

	defineMessage()
	{
		this._messageService.define(this.token, this.idmessage).subscribe(
  			response => {
  				if (response.status == 'success')
  				{
				    console.log(response);
  				}
  				else
  				{
  					console.log(response);
  				}
  			},
  			error => {
  				console.log(<any>error);
  			}
  		);
	}

}

import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { UserService } from '../../services/user.service';
import {User} from '../../models/user';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  providers: [UserService],
  styleUrls: ['./admin-panel.component.css', '../../alert.component.css']
})
export class AdminPanelComponent implements OnInit {

  public title: string;
  public user: User;
  public token;
  public identity;
  public message;
  public type;


  constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService) 
  {
  		this.title = 'Sección del Admin';
		this.identity = this._userService.getIdentity();
  		this.token = this._userService.getToken();
  }

  ngOnInit() 
  {
  		if(this.identity == null || this.identity.role != 'ROLE_ADMIN')
	    {
	        this._router.navigate(['home', { 'message': "No tienes permiso para visualizar esta sección", 'type': "danger" }]);
	    }

      this._route.params.subscribe(data => 
      {
        this.message = data['message'];
        this.type = data['type'];
      })
  }

}

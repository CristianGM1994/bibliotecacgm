import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { UserService } from '../../services/user.service';
import {User} from '../../models/user';
import { Message } from '../../models/message';
import { MessageService } from '../../services/message.service';

@Component({
	
	selector: 'home',
	templateUrl: './home.component.html',
	providers: [UserService, MessageService],
	styleUrls: ['./home.component.css', '../../alert.component.css']
})

export class HomeComponent implements OnInit
{
	public title: string;
	public user: User;
	public status: string;
	public url: any;
	public errors;
	public data;
	public divs;
	public token;
	public identity;
	public message;
	public type;
	public numbermessage: any;

	constructor(private _route: ActivatedRoute, private _router: Router, private _userService: UserService, private _messageService: MessageService)
	{
		this.title = 'Inicio';
		this.identity = this._userService.getIdentity();
  		this.token = this._userService.getToken();

	}

	ngOnInit()
	{
		console.log("Home Cargado");
		if(this.identity == null)
	    {
	        this._router.navigate(['']);
	    }

	    this._route.params.subscribe(data => 
	    {
	    	this.message = data['message'];
	    	this.type = data['type'];
    	})

    	this.getNotSeeMS();
	}

	getNotSeeMS()
	{
	    this._messageService.notseemessage(this.token, this.identity.sub).subscribe(
	      response => {
	      	this.numbermessage = response.messages
	      	console.log(this.numbermessage);
	      },
	      error => {

	        console.log("error");
	      }
	    );
	 }
}
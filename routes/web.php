<?php
use Brotzka\DotenvEditor\DotenvEditor;
use Illuminate\Support\Facades\Storage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/api/register', 'UserController@register');
Route::post('/api/login', 'UserController@login');

//Rutas Users
Route::get('/api/users', 'UserController@index');
Route::resource('/api/user', 'UserController', ['only' => ['show', 'update', 'destroy']]);

//Ruta Recursos Libros
Route::resource('/api/books', 'BookController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
Route::get('/api/books/download/{id}', 'BookController@descargar');
Route::get('/api/books/stream/{id}', 'BookController@stream');
Route::get('/api/books/orderbook/{tipo}/{orden}', 'BookController@orderbook');

Route::post('/api/books/recordpage', 'BookController@storepagenumber');
Route::get('/api/books/recordpage/{tipo}/{nombre}', 'BookController@getpagenumber');
Route::post('/api/books/deleterecordpage', 'BookController@deletepagenumber');

//Ruta Recursos Libros Online
Route::resource('/api/booksonline', 'BookOnlineController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
Route::get('/api/booksonline/search/{id}', 'BookOnlineController@search');
Route::get('/api/booksonline/sendrevision/{id}', 'BookOnlineController@sendrevision');
Route::put('/api/booksonline/updatebookonline/{id}', 'BookOnlineController@updatebookonline');

//Ruta Recurso Genre
Route::resource('/api/genre', 'GenreController', ['only' => ['index', 'store']]);

//Ruta Recurso RevisionBooks
Route::resource('/api/revisionbooks', 'RevisionBookController', ['only' => ['index', 'store', 'update', 'destroy']]);
Route::get('/api/revisionbooks/download/{id}', 'RevisionBookController@descargar');
Route::get('/api/books/orderrevisionbook/{tipo}/{orden}', 'RevisionBookController@ordenrevisionbook');

//Ruta Recurso Comments
Route::resource('/api/comments', 'CommentController', ['only' => ['index', 'show', 'store']]);
Route::get('/api/deletecomment/{id}', 'CommentController@destroy');

//Rutas Likes
Route::get('/api/likes/show/{id}', 'LikeController@show');
Route::get('/api/likes/{id}', 'LikeController@index');
Route::get('/api/likes/{id}/{bookid}', 'LikeController@create');
Route::get('/api/likes/destroy/{id}/{bookid}', 'LikeController@destroy');

//Rutas Messages
Route::post('/api/message', 'MessageController@store');
Route::get('/api/messageindex/{id}', 'MessageController@index');
Route::get('/api/deletemessage/{id}', 'MessageController@destroy');
Route::get('/api/notseemessage/{id}', 'MessageController@dontsee');
Route::get('/api/seemessage/{id}', 'MessageController@see');
Route::get('/api/showmessage/{id}', 'MessageController@show');
Route::get('/api/definemessage/{id}', 'MessageController@define');

Route::get('/api/findbooks/{tipo}/{nombre}', 'BookController@findbook');

Route::post('/api/generateadmin', 'UserController@generateadmin');
Route::post('/api/removeadmin', 'UserController@removeadmin');

Route::post('/api/sendmail', 'EmailController@contact');


Route::get('/api/getbd', function () {
    $env = new DotenvEditor();
   $data = array(
    'status' => 'success',
    'code' => 400,
    'message' => 'Base Obtenida Perfectamente',
    'bbdd' => $env->getValue('DB_DATABASE')
    );
   
   return response()->json($data, 200);
});

Route::get('/api/install/{db}', function ($db) {
    $env = new DotenvEditor();
    
    $env->changeEnv([
            'DB_DATABASE'   => null,
        ]);
    
   Artisan::call('command:createdatabase', ['name' => $db]);
   
   $env->changeEnv([
            'DB_DATABASE'   => $db,
        ]);
   
   return redirect('api/migrate');
});

Route::get('api/migrate', function()
{    
    copy(public_path(). '/safe/19NWKbGihz.png', public_path(). '/cover/19NWKbGihz.png');
    copy(public_path(). '/safe/elprincipito.jpg', public_path(). '/cover/elprincipito.jpg');
    
    copy(public_path(). '/safe/4S5InOAoFp.pdf', public_path(). '/books/4S5InOAoFp.pdf');
    copy(public_path(). '/safe/principito.pdf', public_path(). '/books/principito.pdf');

    copy(public_path(). '/safe/adminavatar.png', public_path(). '/avatar/adminavatar.png');
    copy(public_path(). '/safe/iesavatar.png', public_path(). '/avatar/iesavatar.png');
    copy(public_path(). '/safe/cgmavatar.png', public_path(). '/avatar/cgmavatar.png');
    
   Artisan::call('migrate --seed');
   
   $data = array(
    'status' => 'success',
    'code' => 400,
    'message' => 'Creado Correctamente'
    );
   
   return response()->json($data, 200);
});

Route::get('/api/logs', 'LogController@index');
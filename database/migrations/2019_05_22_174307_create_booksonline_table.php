<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksonlineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booksonline', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id', 11);
            $table->string('title', 200);
            $table->string('autor', 200);
            $table->string('description', 1000);
            $table->string('bookimage', 500)->nullable(true);
            $table->string('bookonline', 10000)->nullable(true);
            $table->timestamps();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('genre_id')->unsigned();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('genre_id')->references('id')->on('genre')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booksonline');
    }
}

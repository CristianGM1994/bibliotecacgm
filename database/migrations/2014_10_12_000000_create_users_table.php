<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {    
            $table->engine = 'InnoDB';
            $table->bigIncrements('id', 11);
            $table->string('name', 200);
            $table->string('nick', 200)->unique();
            $table->string('email', 200)->unique();
            $table->string('password', 500);
            $table->string('image', 10000)->nullable();
            $table->string('role', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

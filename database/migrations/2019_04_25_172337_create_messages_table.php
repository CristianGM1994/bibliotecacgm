<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id', 11);
            $table->string('asunto', 100);
            $table->string('messages', 1000);
            $table->timestamps();
            $table->bigInteger('user_send')->unsigned();
            $table->bigInteger('user_receive')->unsigned();
            $table->tinyInteger('visto');
            
            $table->foreign('user_send')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('user_receive')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('log', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('user_id')->unsigned();
            $table->string('role', 45);
            $table->string('activity', 45);
            $table->string('created_at', 100);
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        
        DB::unprepared('CREATE DEFINER=`root`@`localhost` PROCEDURE `log`(IN `userid` INT(10), IN `rol` VARCHAR(45), IN `ac` VARCHAR(45)) NO SQL INSERT INTO log(`user_id`, `role`, `activity`, `created_at`) VALUES(userid, rol, ac, now());');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log');
    }
}

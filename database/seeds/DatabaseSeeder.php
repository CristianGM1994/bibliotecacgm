<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revisión de claves foráneas
        DB::table('users')->truncate();
        DB::table('genre')->truncate();
        DB::table('books')->truncate();
        DB::table('revisionbooks')->truncate();
        DB::table('likes')->truncate();
        DB::table('comments')->truncate();
        DB::table('messages')->truncate();
        DB::table('log')->truncate();        
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revisión de claves foráneas
        //
        //
        //
        // $this->call(UsersTableSeeder::class);
           $this->call(UserSeeder::class);
           $this->call(GenreSeeder::class);
           $this->call(BookSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Genre')->insert(
        [
            'id' => 1,
            'name' => "Literatura infantil"
        ]);
        
        DB::table('Genre')->insert(
        [
            'id' => 2,
            'name' => "Novela corta"
        ]);
        
        DB::table('Genre')->insert(
        [
            'id' => 3,
            'name' => "Otros"
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            'id' => 1,
            'title' => "El Principito",
            'autor' => "Antoine De Saint Exupery",
            'description' => "Está considerado una de las mejores obras literarias del sigo XX y, aunque está catalogado inicialmente como cuento poético infantil, se recomienda a cualquier persona no importando la edad pues es una obra que te hará reflexionar sobre el sentido de la vida y la naturaleza del ser humano",
            'bookimage' => "elprincipito.jpg",
            'bookurl' => "principito.pdf",
            'created_at' => now(),
            'updated_at' => now(),
            'user_id' => 1,
            'genre_id' => 1
        ]);
        
        DB::table('books')->insert([
            'id' => 2,
            'title' => "Documentación Trabajo TFG",
            'autor' => "Cristian García",
            'description' => "Trabajo TFG. Cristian García Martín Presentación 20/06/19.",
            'bookimage' => "19NWKbGihz.png",
            'bookurl' => "4S5InOAoFp.pdf",
            'created_at' => now(),
            'updated_at' => now(),
            'user_id' => 3,
            'genre_id' => 3
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'id' => 1,
            'name' => "Administrador",
            'nick' => "Admin",
            'email' => "admin@admin.com",
            'password' => "277bd398664ea2dfcab76d9a3955bff2f09ceabadf280c013e5b1b8109736046",
            'image' => "adminavatar.png",
            'role' => "ROLE_ADMIN",
            'created_at' => now(),
            'updated_at' => now(),
        ]);
         
        DB::table('users')->insert([
            'id' => 2,
            'name' => "IESSanSebastian",
            'nick' => "iessansebastian",
            'email' => "ies@admin.com",
            'password' => "277bd398664ea2dfcab76d9a3955bff2f09ceabadf280c013e5b1b8109736046",
            'image' => "iesavatar.png",
            'role' => "ROLE_USER",
            'created_at' => now(),
            'updated_at' => now(),
        ]);
         
        DB::table('users')->insert([
            'id' => 3,
            'name' => "Cristian",
            'nick' => "CristianGM",
            'email' => "cristian.gm.1994@gmail.com",
            'password' => "277bd398664ea2dfcab76d9a3955bff2f09ceabadf280c013e5b1b8109736046",
            'image' => "cgmavatar.png",
            'role' => "ROLE_USER",
            'created_at' => now(),
            'updated_at' => now(),
        ]);
         
         
    }
}

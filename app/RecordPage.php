<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecordPage extends Model
{
    protected $table = 'recordpages';
    
    //Relaciones Muchos a Uno
    public function book()
    {
        return $this->belongsTo('App\Book', 'book_id');
    }
    
    //Relaciones Muchos a Uno
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}

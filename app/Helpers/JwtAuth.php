<?php
namespace App\Helpers;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use App\User;

class JwtAuth
{
    public $key;
    
    public function __construct() {
        $this->key = 'bibliotecacgm17-2265655656844';
    }
    
    public function signup($email, $password, $getToken=null)
    {
        $user = User::where(
                array(
                    'email' => $email,
                    'password' => $password
                ))->first();
        
        $signup = false;
        
        if(is_object($user))
        {
            $signup = true;
        }
        
        if($signup == true)
        {
            //Generar Token y Devolverlo
            
            $token = array(
                'sub' => $user->id,
                'name' => $user->name,
                'nick' => $user->nick,
                'email' => $user->email,
                'image' => $user->image,
                'role' => $user->role,
                'created_at' => $user->created_at,
                'updated_at' => $user->updated_at,
                'iat' => time(),
                'exp' => time() + (7*24*60*60)
            );
            
            
            //Cifrado
            $jwt = JWT::encode($token, $this->key, 'HS256');
            
            //Decodificación
            $decoded = JWT::decode($jwt, $this->key, array('HS256'));
            
            if(is_null($getToken))
            {
                return $jwt;
            }
            else
            {
                DB::select('call log(?,?,?)',array($user->id, $user->role, "Login"));
                return $decoded;
            }
        }
        else
        {
            //Generar un Error
            return array('status' => 'Error', 'message' => '¡El Email o la Contraseña no existen en nuestros registros!');
        }
    }
    
    public function checkToken($jwt, $getIdentity = false)
    {
        $auth = false;
        
        try
        {
            $decoded = JWT::decode($jwt, $this->key, array('HS256'));
        } 
        catch (\UnexpectedValueException $e) 
        {
            $auth = false;
        }
        catch (\DomainException $e) 
        {
            $auth = false;
        }
        
        if(isset($decoded) && is_object($decoded) && isset($decoded->sub))
        {
            $auth = true;
        }
        else
        {
            $auth = false;
        }
        
        if($getIdentity)
        {
            return $decoded;
        }
        
        return $auth;
    }
}



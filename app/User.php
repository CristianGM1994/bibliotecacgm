<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function book()
    {
        return $this->hasMany('App\Book');
    }
    
     public function bookonline()
    {
        return $this->hasMany('App\BookOnline');
    }
    
    public function revisionbook()
    {
        return $this->hasMany('App\RevisionBook');
    }
    
    public function message()
    {
        return $this->hasMany('App\Message');
    }
    
    public function comment()
    {
        return $this->hasMany('App\Comment');
    }
    
    public function likes()
    {
        return $this->hasMany('App\Like');
    }
}

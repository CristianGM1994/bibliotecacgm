<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
     protected $table = 'log';
     //Relaciones Muchos a Uno
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}

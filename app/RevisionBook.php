<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RevisionBook extends Model
{
    protected $table = 'revisionbooks';
    
    //Relaciones
    
    //Relaciones Muchos a Uno
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    
    //Relaciones Muchos a Uno
    public function genre()
    {
        return $this->belongsTo('App\Genre', 'genre_id');
    }
    
}

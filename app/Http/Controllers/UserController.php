<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use Illuminate\Support\Facades\DB;

use App\Helpers\JwtAuth;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    public function register(Request $request)
    {
        //Recoger POST
        
        $json = $request->input('json', null);    
        $params = json_decode($json);
        $params_array = json_decode($json, true);
        
        $validate = \Validator::make($params_array , [
                'name' => 'required|min:4',
                'nick' => 'required|min:4', 'unique:users',
                'email' => 'required|min:4', 'unique:users',
                'password' => 'required|confirmed'
            ]);
        
        if($validate->fails())
        {
            $data = ($validate->errors());
            return response()->json($data, 400);
        }
        
        $nick = $params->nick;
        $email = $params->email;
        $name = $params->name;
        $image = $params->image;
        $role = 'ROLE_USER';
        $password = $params->password;
        
        $isset_email = User::where('email', '=' , $email)->first();
        $isset_nick = User::where('nick', '=' , $nick)->first();
            
        if(is_null($isset_email) && is_null($isset_nick))
        {
                if(!is_null($image))
                {
                    //Imagenes PNG guardar
                    /*$image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = str_random(10).'.'.'png';
                    \File::put(storage_path(). '/' . $imageName, base64_decode($image));*/
                    
                    if(strpos($image, 'data:image/png;base64') !== false)
                    {
                        $image = str_replace('data:image/png;base64,', '', $image);
                        $image = str_replace(' ', '+', $image);
                        $imageName = str_random(10).'.'.'png';
                        \File::put(public_path(). '/avatar/' . $imageName, base64_decode($image));
                    }
                    
                    if(strpos($image, 'data:image/jpeg;base64') !== false)
                    {
                        $image = str_replace('data:image/jpeg;base64,', '', $image);
                        $image = str_replace(' ', '+', $image);
                        $imageName = str_random(10).'.'.'jpg';
                        \File::put(public_path(). '/avatar/' . $imageName, base64_decode($image));
                    }
                    
                    /*$image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = str_random(10).'.'.'pdf';
                    \File::put(storage_path(). '/' . $imageName, base64_decode($image));*/
                } 
                
                //Crear el usuario
                $user = new User();
                $user->name = $name;
                $user->nick = $nick;
                $user->email = $email;
                if(isset($imageName))
                {
                    $user->image = $imageName;
                }
                $user->role = $role;

                //Generar contraseña segura
                $pwd = hash('sha256', $password);
                $user->password = $pwd;

                //Guardar el usuario
                $user->save();
                 $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => '¡Usuario registrado con exito!'
                );
        }
        elseif (!is_null($isset_email) || !is_null($isset_nick))
        {
            //No guardarlo porque ya existe
                    $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario duplicado (Nombre de usuario o Email en uso)'
                    );
        }
        else
        {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no creado'
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function login(Request $request)
    {
        $jwtAuth = new JwtAuth();
        
        //Recibir datos POST
        
        $json = $request->input('json', null);
        $params = json_decode($json);
        
        $email = (!is_null($json) && isset($params->email)) ? $params->email : null;
        $password = (!is_null($json) && isset($params->password)) ? $params->password : null;
        $getToken = (!is_null($json) && isset($params->gettoken)) ? $params->gettoken : null;
    
        //Cifrar la password
        $pwd = hash('sha256', $password);
        
        if(!is_null($email) && !is_null($password) && ($getToken == null || $getToken == "false"))
        {
            $signup = $jwtAuth->signup($email, $pwd);
            
        }
        elseif($getToken != null)
        {
            $signup = $jwtAuth->signup($email, $pwd, $getToken);
            
        }
        else
        {
            $signup =  array(
                'status' => 'error',
                'message' => 'Envia tus datos por POST'
            );
        }
        
        return response()->json($signup, 200);
    }
    
    public function index(Request $request)
    {
        $user = User::all();
            return response()->json(array(
                'user' => $user,
                'status' => 'success'
            ), 200);
    }
    
    public function show(Request $request, $id)
    {       
            $user = User::find($id);
            if(is_object($user))
            {
                return response()->json(array(
                    'user' => $user->load('Book'),
                    'status' => 'success'
                ), 200);
            }
            else
            {
                return response()->json(array(
                'message' => 'El usuario no existe',
                'status' => 'error'
            ), 200); 
            }
    }
    
    public function update($id, Request $request)
    {
        $hash = $request->header('Authorization', null);
                
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Parametros POST
            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);
            
            //Validar los datos
             $validate = \Validator::make($params_array , [
                'name' => 'required|min:4',
                'nick' => 'required|min:4|unique:users,nick,'.$params->id,
                'email' => 'required|min:4|unique:users,email,'.$params->id,
                'password' => 'confirmed'
            ]);
            
            if($validate->fails())
            {
                return response()->json($validate->errors(), 400);
            }
               
            $imageuser = User::find($id);

            //Actualizar Coche
            unset($params_array['id']);
            unset($params_array['created_at']);
            unset($params_array['book']);
            
            if($params_array['image'] == $imageuser->image)
            {
                unset($params_array['image']);
            }
            else
            {
                //Imagenes PNG guardar
                    /*$image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = str_random(10).'.'.'png';
                    \File::put(storage_path(). '/' . $imageName, base64_decode($image));*/
                    
                    if(strpos($params_array['image'], 'data:image/png;base64') !== false)
                    {
                        $params_array['image'] = str_replace('data:image/png;base64,', '', $params_array['image']);
                        $params_array['image'] = str_replace(' ', '+', $params_array['image']);
                        $imageName = str_random(10).'.'.'png';
                        \File::put(public_path(). '/avatar/' . $imageName, base64_decode($params_array['image']));
                    }
                    
                    if(strpos($params_array['image'], 'data:image/jpeg;base64') !== false)
                    {
                        $params_array['image'] = str_replace('data:image/jpeg;base64,', '', $params_array['image']);
                        $params_array['image'] = str_replace(' ', '+', $params_array['image']);
                        $imageName = str_random(10).'.'.'jpg';
                        \File::put(public_path(). '/avatar/' . $imageName, base64_decode($params_array['image']));
                    }
                    
                    /*$image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = str_random(10).'.'.'pdf';
                    \File::put(storage_path(). '/' . $imageName, base64_decode($image));*/
                    
                    if(!is_null($imageuser->image))
                    {
                        $file_path = public_path(). '/avatar/' . $imageuser->image;
                        unlink($file_path);
                    }
                    
                    $params_array['image'] = $imageName;
            }
            
            if(!isset($params_array['password']) || empty($params_array['password']))
            {
                unset($params_array['password']);
                unset($params_array['password_confirmation']);
            }
            else
            {
                $pwd = hash('sha256', $params_array['password']);
                $params_array['password'] = $pwd;
                unset($params_array['password_confirmation']);
            }
            
            $user = User::where('id', $id)->update($params_array);
            
            $imageuser = User::find($id);
            
            $identidad = array(
                'sub' => $imageuser->id,
                'name' => $imageuser->name,
                'nick' => $imageuser->nick,
                'email' => $imageuser->email,
                'image' => $imageuser->image,
                'role' => $imageuser->role,
                'created_at' => $imageuser->created_at,
                'updated_at' => $imageuser->updated_at,
                'iat' => time(),
                'exp' => time() + (7*24*60*60)
            );
            
            $data = array(
                    'user' => $identidad,
                    'status' => 'success',
                    'code' => 200,
                );
            
            DB::select('call log(?,?,?)',array($imageuser->id, $imageuser->role, "Actualización Usuario"));
        }
        else
        {
            //Devolver Error
             $data = array(
                    'message' => 'No se ha podido realizar la actualización',
                    'status' => 'error',
                    'code' => 400,
                );
        }
        
         return response()->json($data, 200);
    }
    
    public function destroy(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            $user = User::find($id);
            
            DB::select('call log(?,?,?)',array($user->id, $user->role, "Usuario Borrado"));
            
            $user->delete();

            $data = array(
                'user' => $user,
                'status' => 'success',
                'code' => 200
            );
            

        }
        else
        {
            $data = array(
                'status' => 'error',
                'message' => "No se ha podido eliminar el usuario",
                'code' => 400
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function generateadmin(Request $request)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
             //Recoger Parametros POST
            $json = $request->input('json', null);
            $params = json_decode($json);
            
            $user = User::find($params);
            
            $user->role = 'ROLE_ADMIN';
            
            $user->update();
            
            //Devolver Error
             $data = array(
                    'message' => 'Cambio realizado con exito',
                    'status' => 'success',
                    'code' => 200,
                );
             
             DB::select('call log(?,?,?)',array($user->id, $user->role, "Dar Admin"));
             
        }
        else
        {
            $data = array(
                'status' => 'error',
                'message' => "No se ha podido generar el Administrador",
                'code' => 400
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function removeadmin(Request $request)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
             //Recoger Parametros POST
            $json = $request->input('json', null);
            $params = json_decode($json);
            
            $user = User::find($params);
            
            $user->role = 'ROLE_USER';
            
            $user->update();
            
            //Devolver Error
             $data = array(
                    'message' => 'Cambio realizado con exito',
                    'status' => 'success',
                    'code' => 200,
                );
             
             
             DB::select('call log(?,?,?)',array($user->id, $user->role, "Revocar Admin"));
             
        }
        else
        {
            $data = array(
                'status' => 'error',
                'message' => "No se ha podido quitar el rango de Administrador",
                'code' => 400
            );
        }
        
        return response()->json($data, 200);
    }
}

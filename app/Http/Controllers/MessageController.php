<?php

namespace App\Http\Controllers;

use App\Message;

use App\Helpers\JwtAuth;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Datos por POST
           
            $messages = Message::where('user_receive', $id)
                    ->orderBy('created_at', 'desc')
                    ->get()
                    ->load('User')
                    ->load('userreceive');
            
            $data = array(
                'messages' => $messages,
                'status' => 'success',
                'code' => 200
            );
        }
        else
        {
           $data = array(
                'message' => 'No se ha podido listar los mensajes',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function store(Request $request)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Datos por POST
            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);
            
            //Conseguir el usuario identificado
            $user = $jwtAuth->checkToken($hash, true);
            
            //FALTA METER IMAGEN Y URL
            $validate = \Validator::make($params_array , [
                'asunto' => 'required',
                'mensaje' => 'required',
                'user_send' => 'required',
                'user_receive' => 'required'
            ]);
            
            if($validate->fails())
            {
                return response()->json($validate->errors(), 400);
            }
            
            //RECOGER VALORES
         
            $asunto = $params->asunto;
            $mensaje = $params->mensaje;
            $user_send = $params->user_send;
            $user_receive = $params->user_receive;
            
            
            //CREAR OBJETO Y GUARDARLO
            $message = new Message();
            $message->asunto = $asunto;
            $message->messages = $mensaje;
            $message->user_send = $user_send;
            $message->user_receive = $user_receive;
            $message->visto = 0;
            
            $message->save();
            
            
            $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => '¡Mensaje registrado con exito!'
                );
        }
        else
        {
           $data = array(
                'message' => 'No se ha podido registrar el registrado',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
        
    }
    
    public function destroy(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            $message = Message::find($id);
            $message->delete();
            $data = array(
                'message' => $message,
                'status' => 'success',
                'code' => 200
            );
        }
        else
        {
            $data = array(
                'status' => 'error',
                'message' => 'Mensaje no borrado',
                'code' => 400
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function dontsee(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Datos por POST    
            $messages = Message::where('user_receive', $id)
                    ->where('visto', 0)
                    ->count();
            
            $data = array(
                'messages' => $messages,
                'status' => 'success',
                'code' => 200
            );
        }
        else
        {
           $data = array(
                'message' => 'No se ha podido listar los mensajes',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function see(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Datos por POST    
            $messages = Message::where('user_receive', $id)
                    ->where('visto', 0)
                    ->count();
            
            $data = array(
                'messages' => $messages,
                'status' => 'success',
                'code' => 200
            );
        }
        else
        {
           $data = array(
                'message' => 'No se ha podido listar los mensajes',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
    }
    
   public function show(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Datos por POST    
             $messages = Message::where('id', $id)
                    ->first()
                    ->load('User');
            
            $data = array(
                'messages' => $messages,
                'status' => 'success',
                'code' => 200
            );
        }
        else
        {
           $data = array(
                'message' => 'No se ha podido listar los mensajes',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
    }
    
   public function define(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Datos por POST    
             $messages = Message::where('id', $id)
                    ->first();
             
             $messages->visto = 1;
             
             $messages->update();
            
            $data = array(
                'messages' => $messages,
                'status' => 'success',
                'code' => 200
            );
        }
        else
        {
           $data = array(
                'message' => 'No se ha podido listar los mensajes',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
    }
}

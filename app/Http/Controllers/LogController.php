<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\JwtAuth;

use App\Log;


class LogController extends Controller
{
    public function index(Request $request)
    {
        $hash = $request->header('Authorization', null);
                
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            $logs = Log::orderBy('created_at', 'desc')
                    ->get()
                    ->load('User');
            return response()->json(array(
                'listado' => $logs,
                'status' => 'success'
            ), 200);
        }
        else
        {
            //Devolver Error
             $data = array(
                    'message' => 'No se ha podido listar el LOG',
                    'status' => 'error',
                    'code' => 400,
                );
        }
        
         return response()->json($data, 200);
    }
}

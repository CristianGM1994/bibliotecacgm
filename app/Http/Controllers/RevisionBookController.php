<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\RevisionBook;
use App\Book;

use App\Helpers\JwtAuth;

class RevisionBookController extends Controller
{
    
    public function index(Request $request)
    {      
        $books = RevisionBook::all()
                ->load('User')
                ->load('Genre');
        return response()->json(array(
            'books' => $books,
            'status' => 'success'
        ), 200);
        
    }
    
    public function store(Request $request)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Datos por POST
            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);
            
            //Conseguir el usuario identificado
            $user = $jwtAuth->checkToken($hash, true);
            
            //FALTA METER IMAGEN Y URL
            $validate = \Validator::make($params_array , [
                'title' => 'required',
                'autor' => 'required',
                'description' => 'required',
                'bookimage' => 'required',
                'bookurl' => 'required',
                'genre_id' => 'required'
            ]);
            
            if($validate->fails())
            {
                return response()->json($validate->errors(), 400);
            }
            
            //RECOGER VALORES
         
            $title = $params->title;
            $autor = $params->autor;
            $description = $params->description;
            $bookimage = $params->bookimage;
            $bookurl = $params->bookurl;
            $user_id = $user->sub;
            $genre_id = $params->genre_id;
            
            
            //ASIGNAR NOMBRE Y GUARDAR PORTADA Y LIBRO EN CARPETA PUBLICA
            if(strpos($bookimage, 'data:image/png;base64') !== false)
            {
                $bookimage = str_replace('data:image/png;base64,', '', $bookimage);
                $bookimage = str_replace(' ', '+', $bookimage);
                $imageName = str_random(10).'.'.'png';
                \File::put(public_path(). '/cover/' . $imageName, base64_decode($bookimage));
            }

            if(strpos($bookimage, 'data:image/jpeg;base64') !== false)
            {
                $bookimage = str_replace('data:image/jpeg;base64,', '', $bookimage);
                $bookimage = str_replace(' ', '+', $bookimage);
                $imageName = str_random(10).'.'.'jpg';
                \File::put(public_path(). '/cover/' . $imageName, base64_decode($bookimage));
            }
            
            if(strpos($bookurl, 'data:application/octet-stream;base64') !== false)
            {
                $bookurl = str_replace('data:application/octet-stream;base64,', '', $bookurl);
                $bookurl = str_replace(' ', '+', $bookurl);
                $urlName = str_random(10).'.'.'doc';
                \File::put(public_path(). '/books/' . $urlName, base64_decode($bookurl));
            }
            
            if(strpos($bookurl, 'data:application/pdf;base64') !== false)
            {
                $bookurl = str_replace('data:application/pdf;base64,', '', $bookurl);
                $bookurl = str_replace(' ', '+', $bookurl);
                $urlName = str_random(10).'.'.'pdf';
                \File::put(public_path(). '/books/' . $urlName, base64_decode($bookurl));
            }
            
            //CREAR OBJETO Y GUARDARLO
            $book = new RevisionBook();
            $book->title = $title;
            $book->autor = $autor;
            $book->description = $description;
            $book->bookimage = $imageName;
            $book->bookurl = $urlName;
            $book->user_id = $user_id;
            $book->genre_id = $genre_id;
            
            $book->save();
            
            
            $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => '¡Libro registrado con exito!'
                );
            
            DB::select('call log(?,?,?)',array($user_id, 'ROLE_USER', "Subida Libro"));
        }
        else
        {
           $data = array(
                'message' => 'No se ha podido registrar el libro',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
        
    }
    
    public function update(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Datos por POST
            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);

            //Actualizar el coche
            $revisionbook = RevisionBook::find($id);
            
            $newbook = new Book();
            
            $newbook->title = $revisionbook->title;
            $newbook->autor = $revisionbook->autor;
            $newbook->description = $revisionbook->description;
            $newbook->bookimage = $revisionbook->bookimage;
            $newbook->bookurl = $revisionbook->bookurl;
            $newbook->user_id = $revisionbook->user_id;
            $newbook->genre_id = $revisionbook->genre_id;
            
            $newbook->save();
            
            $revisionbook->delete();
            
            $data = array(
                'book' => $newbook,
                'status' => 'success',
                'code' => 200
            );
            
            DB::select('call log(?,?,?)',array(1, 'ROLE_ADMIN', "Aprobar Libro"));

        }
        else
        {
            //Devolver Error
            echo("NO AUTENTICADO");
            die();
        }
        
        return response()->json($data, 200);
    }
    
    public function destroy(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            $revisionbook = RevisionBook::find($id);
            
            $file_cover = public_path(). '/cover/' . $revisionbook->bookimage;
            unlink($file_cover);
            
            $file_book = public_path(). '/books/' . $revisionbook->bookurl;
            unlink($file_book);

            $revisionbook->delete();
            
            $data = array(
                'book' => $revisionbook,
                'status' => 'success',
                'code' => 200
            );
            
            DB::select('call log(?,?,?)',array(1, 'ROLE_ADMIN', "Declinar Libro"));
        }
        else
        {
            $data = array(
                'status' => 'error',
                'message' => 'Eliminación incorrecta',
                'code' => 400
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function descargar($id)
    {
        $books = RevisionBook::find($id);
        return response()->download(public_path('books/' . $books->bookurl));
    }
    
        public function ordenrevisionbook($tipo, $orden)
    {
        $books = RevisionBook::orderBy($tipo, $orden)
                ->get()
                ->load('User')
                ->load('Genre');
        return response()->json(array(
            'books' => $books,
            'status' => 'success'
        ), 200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Like;

use App\Helpers\JwtAuth;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Response;

class LikeController extends Controller
{
    public function index(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            $likes = Like::where('user_id', $id)
                    ->pluck('book_id');
            //$likes = DB::table('likes')->get(); 
            return response()->json(array(
                'likes' => $likes,
                'status' => 'success'
            ), 200);
        }
        else
        {
            $data = array(
                'message' => 'No se ha podido listar los Likes',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function show(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            $likes = Like::where('user_id', $id)
                    ->get()
                    ->load('book');
            
            $likeuser = $likes
                    ->pluck('book_id');

            return response()->json(array(
                'likes' => $likes,
                'likesuser' => $likeuser,
                'status' => 'success'
            ), 200);
        }
        else
        {
            $data = array(
                'message' => 'No se ha podido listar los Likes',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function create($id, $idbook, Request $request)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            $like = new Like();

            $like->user_id = $id;

            $like->book_id = $idbook;
            
            $like->save();
            
            $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => '¡Like registrado con exito!'
                );
        }
        else
        {
            $data = array(
                'message' => 'No se ha podido registrar el like',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function destroy($id, $idbook, Request $request)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            $like = Like::where([['user_id', $id], ['book_id', $idbook]])->first();
            $like->delete();
            return response()->json(array(
                'likes' => $like,
                'status' => 'success'
            ), 200);
        }
        else
        {
             $data = array(
                'message' => 'No se ha podido borrar el DisLike',
                'status' => 'error',
                'code' => 300
            );
        }
        
         return response()->json($data, 200);
    }
}

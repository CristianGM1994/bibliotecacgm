<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Genre;

use App\Helpers\JwtAuth;

class GenreController extends Controller
{
    public function index(Request $request)
    {      
            $genre = Genre::all();
            return response()->json(array(
                'genre' => $genre,
                'status' => 'success'
            ), 200);
    }
    
    public function store(Request $request)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Datos por POST
            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);
            
            //Conseguir el usuario identificado
            $user = $jwtAuth->checkToken($hash, true);
            
            //FALTA METER IMAGEN Y URL
            $validate = \Validator::make($params_array , [
                'name' => 'required'
            ]);
            
            if($validate->fails())
            {
                return response()->json($validate->errors(), 400);
            }
            
            //RECOGER VALORES
         
            $name = $params->name;
            
            
            $exist = Genre::where('name', $name)->first();
            
            if($exist)
            {
                $data = array(
                'status' => 'danger',
                'code' => 200,
                'error' => '¡Ya existe un genero con este nombre!'
                );
            }
            else
            {
                //CREAR OBJETO Y GUARDARLO
                $genre = new Genre();
                $genre->name = $name;

                $genre->save();


                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => '¡Genero registrado con exito!'
                    );
            }
        }
        else
        {
           $data = array(
                'message' => 'No se ha podido registrar el libro',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
        
    }
}


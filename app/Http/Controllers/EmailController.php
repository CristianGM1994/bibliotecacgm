<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;

use Mail;

use App\Book;

use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function contact(Request $request)
    {
        $hash = $request->header('Authorization', null);
                
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {        
            $subject = "¡Alguien te envió un libro por correo!";
            $json = $request->input('json', null);
            $params = json_decode($json);           
            $libro = Book::find($params->id);
            $mensaje = $params->mensaje;
            $book = $libro->bookurl;
            
            $for = $params->email;
            Mail::send('email',['libro'=>$libro,'messages'=>$mensaje], function($msj) use($subject,$for,$book){
                $msj->from("proyectot-fgcgm@gmail.com", "BibliotecaCGM");
                $msj->subject($subject);
                $msj->to($for);
                $msj->attach(public_path(). '/books/' . $book);
        });
        
             $data = array(
                    'message' => '¡Libro Enviado Con Exito!',
                    'status' => 'success',
                    'code' => 200,
                );
        }
        else
        {
            //Devolver Error
             $data = array(
                    'message' => 'No se ha podido realizado el envio',
                    'status' => 'error',
                    'code' => 400,
                );
        }
        
        return response()->json($data, 200);
    }
}

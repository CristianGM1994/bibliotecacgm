<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BookOnline;

use App\Helpers\JwtAuth;

use App\RevisionBook;

use App;

use Illuminate\Support\Facades\DB;

class BookOnlineController extends Controller
{
    public function index()
    {      
        $books = BookOnline::all()
                ->load('User')
                ->load('Genre');
        return response()->json(array(
            'books' => $books,
            'status' => 'success'
        ), 200);
        
    }
    
    public function search(Request $request, $id)
    {
        $books = BookOnline::where('user_id', '=', $id)->orderBy('created_at', 'DESC')
                    ->get()
                    ->load('User')
                    ->load('Genre');
            
            return response()->json(array(
                'books' => $books,
                'status' => 'success'
            ), 200);
    }
    
    public function show(Request $request, $id)
    {
        $books = BookOnline::find($id)
               ->load('User')
               ->load('Genre');

       return response()->json(array(
           'books' => $books,
           'status' => 'success'
       ), 200);
    }
    
    public function store(Request $request)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Datos por POST
            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);
            
            //Conseguir el usuario identificado
            $user = $jwtAuth->checkToken($hash, true);
            
            //FALTA METER IMAGEN Y URL
            $validate = \Validator::make($params_array , [
                'title' => 'required',
                'autor' => 'required',
                'description' => 'required',
                'user_id' => 'required',
                'genre_id' => 'required'
            ]);
            
            if($validate->fails())
            {
                return response()->json($validate->errors(), 400);
            }
            
            //RECOGER VALORES
         
            $title = $params->title;
            $autor = $params->autor;
            $description = $params->description;
            $bookimage = $params->bookimage;
            $user_id = $user->sub;
            $genre_id = $params->genre_id;
            
            
            //ASIGNAR NOMBRE Y GUARDAR PORTADA Y LIBRO EN CARPETA PUBLICA
            
            if($bookimage != '')
            {              
                if(strpos($bookimage, 'data:image/png;base64') !== false)
                {
                    $bookimage = str_replace('data:image/png;base64,', '', $bookimage);
                    $bookimage = str_replace(' ', '+', $bookimage);
                    $imageName = str_random(10).'.'.'png';
                    \File::put(public_path(). '/cover/' . $imageName, base64_decode($bookimage));
                }

                if(strpos($bookimage, 'data:image/jpeg;base64') !== false)
                {
                    $bookimage = str_replace('data:image/jpeg;base64,', '', $bookimage);
                    $bookimage = str_replace(' ', '+', $bookimage);
                    $imageName = str_random(10).'.'.'jpg';
                    \File::put(public_path(). '/cover/' . $imageName, base64_decode($bookimage));
                }
            }
                         
            
            //CREAR OBJETO Y GUARDARLO
            $book = new BookOnline();
            $book->title = $title;
            $book->autor = $autor;
            $book->description = $description;
            if(isset($imageName))
            {
                $book->bookimage = $imageName;
            }
            else
            {
                $book->bookimage = null;
            }
            
            $book->user_id = $user_id;
            $book->genre_id = $genre_id;
            
            $book->save();
            
            
            $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => '¡Libro registrado con exito!'
                );
            
            DB::select('call log(?,?,?)',array($user_id, $user->role, "Creación Libro Online"));
        }
        else
        {
           $data = array(
                'message' => 'No se ha podido registrar el libro online',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
        
    }
    
    public function update(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
                
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Parametros POST
            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);
            
            //Validar los datos
             $validate = \Validator::make($params_array , [
                'title' => 'required',
                'autor' => 'required',
                'description' => 'required',
                'user_id' => 'required',
                'genre_id' => 'required'
            ]);
            
            if($validate->fails())
            {
                return response()->json($validate->errors(), 400);
            }
               
            $imagebook = BookOnline::find($id);

            //Actualizar Coche
            unset($params_array['id']);
            unset($params_array['created_at']);
            unset($params_array['user']);
             unset($params_array['genre']);
            
            if($params_array['bookimage'] == $imagebook->bookimage)
            {
                unset($params_array['bookimage']);
            }
            else
            {
                //Imagenes PNG guardar
                    /*$image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = str_random(10).'.'.'png';
                    \File::put(storage_path(). '/' . $imageName, base64_decode($image));*/
                    
                    if(strpos($params_array['bookimage'], 'data:image/png;base64') !== false)
                    {
                        $params_array['bookimage'] = str_replace('data:image/png;base64,', '', $params_array['bookimage']);
                        $params_array['bookimage'] = str_replace(' ', '+', $params_array['bookimage']);
                        $imageName = str_random(10).'.'.'png';
                        \File::put(public_path(). '/cover/' . $imageName, base64_decode($params_array['bookimage']));
                    }
                    
                    if(strpos($params_array['bookimage'], 'data:image/jpeg;base64') !== false)
                    {
                        $params_array['bookimage'] = str_replace('data:image/jpeg;base64,', '', $params_array['bookimage']);
                        $params_array['bookimage'] = str_replace(' ', '+', $params_array['bookimage']);
                        $imageName = str_random(10).'.'.'jpg';
                        \File::put(public_path(). '/cover/' . $imageName, base64_decode($params_array['bookimage']));
                    }
                    
                    /*$image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = str_random(10).'.'.'pdf';
                    \File::put(storage_path(). '/' . $imageName, base64_decode($image));*/
                    
                    if(!is_null($imagebook->bookimage))
                    {
                        $file_path = public_path(). '/cover/' . $imagebook->bookimage;
                        unlink($file_path);
                    }
                    
                    $params_array['bookimage'] = $imageName;
            }
            
            
            $libro = BookOnline::where('id', $id)->update($params_array);
            
            
            $data = array(
                    'book' => $libro,
                    'status' => 'success',
                    'code' => 200,
                );
        }
        else
        {
            //Devolver Error
             $data = array(
                    'message' => 'No se ha podido realizar la actualización',
                    'status' => 'error',
                    'code' => 400,
                );
        }
        
        return response()->json($data, 200);
            
    }
    
    public function destroy(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            $bookonline = BookOnline::find($id);
            
            if(isset($bookonline->bookimage))
            {
                $file_cover = public_path(). '/cover/' . $bookonline->bookimage;
                unlink($file_cover);
            }
            
            $bookonline->delete();
            
            $data = array(
                'book' => $bookonline,
                'status' => 'success',
                'code' => 200
            );
            
            DB::select('call log(?,?,?)',array(1, 'ROLE_ADMIN', "Eliminar Libro Online"));
        }
        else
        {
            $data = array(
                'status' => 'error',
                'message' => 'Eliminación incorrecta',
                'code' => 400
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function sendrevision(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            $bookonline = BookOnline::find($id);
            
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($bookonline->bookonline);
            $bookName = str_random(10).'.'.'pdf';
            $pdf->save(public_path(). '/books/' . $bookName);
            
            $revisionbook = new RevisionBook();
            
            $revisionbook->title = $bookonline->title;
            $revisionbook->autor = $bookonline->autor;
            $revisionbook->description = $bookonline->description;
            $revisionbook->bookimage = $bookonline->bookimage;
            $revisionbook->bookurl = $bookName;
            $revisionbook->user_id = $bookonline->user_id;
            $revisionbook->genre_id = $bookonline->genre_id;
            
            $revisionbook->save();
            
            $bookonline->delete();
            
            
            $data = array(
                'status' => 'success',
                'code' => 200
            );
            
            DB::select('call log(?,?,?)',array(1, 'ROLE_ADMIN', "Mandar a Revisión Libro Online"));
        }
        else
        {
            $data = array(
                'status' => 'error',
                'message' => 'Sin Token',
                'code' => 400
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function updatebookonline(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
                
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Parametros POST
            $json = $request->input('json', null);
            $params = json_decode($json);
           
            
            $bookonline = BookOnline::find($id);
            
            $bookonline->bookonline = $params->bookonline;
            
            $bookonline->update();
            
            
            $data = array(
                    'book' => $bookonline,
                    'status' => 'success',
                    'code' => 200,
                );
        }
        else
        {
            //Devolver Error
             $data = array(
                    'message' => 'No se ha podido realizar la actualización',
                    'status' => 'error',
                    'code' => 400,
                );
        }
        
        return response()->json($data, 200);
    }
}

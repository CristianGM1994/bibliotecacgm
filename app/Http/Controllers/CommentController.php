<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Comment;

use App\Helpers\JwtAuth;

class CommentController extends Controller
{
    
    public function index(Request $request)
    {      
        $comments = Comment::all()
                ->load('User')
                ->load('Book');
        return response()->json(array(
            'comments' => $comments,
            'status' => 'success'
        ), 200);
        
    }
    
    public function show(Request $request, $id)
    {
            $comments = Comment::where('book_id', '=', $id)->orderBy('created_at', 'DESC')
                    ->get()
                    ->load('User');
            return response()->json(array(
                'comments' => $comments,
                'status' => 'success'
            ), 200);
    }
    
    public function store(Request $request)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Datos por POST
            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);
            
            //Conseguir el usuario identificado
            $user = $jwtAuth->checkToken($hash, true);
            
            $validate = \Validator::make($params_array , [
                'comment' => 'required'
            ]);
            
            if($validate->fails())
            {
                return response()->json($validate->errors(), 400);
            }
            
            //RECOGER VALORES
         
            $comentario = $params->comment;
            $user_id = $params->sub;
            $book_id = $params->book_id;
            
            //CREAR OBJETO Y GUARDARLO
            $comment = new Comment();
            $comment->comment = $comentario;
            $comment->user_id = $user_id;
            $comment->book_id = $book_id;
            
            $comment->save();
            
            
            $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => '¡Comentario registrado con exito!'
                );
        }
        else
        {
           $data = array(
                'message' => 'No se ha podido registrar el comentario',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
        
    }
    
    public function destroy(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            $message = Comment::find($id);
            $message->delete();
            $data = array(
                'message' => $message,
                'status' => 'success',
                'code' => 200
            );
        }
        else
        {
            $data = array(
                'status' => 'error',
                'message' => 'Comentario no borrado',
                'code' => 400
            );
        }
        
        return response()->json($data, 200);
    }
}

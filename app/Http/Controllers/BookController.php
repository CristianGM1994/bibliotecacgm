<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Book;

use App\RecordPage;

use Illuminate\Support\Facades\DB;

use App\Helpers\JwtAuth;

use Illuminate\Support\Facades\Response;

use Bluora\GoogleBooksApi\GoogleBooksApi;

class BookController extends Controller
{
    public function index()
    {      
        $books = Book::all()
                ->load('User')
                ->load('Like')
                ->load('Genre');
        return response()->json(array(
            'books' => $books,
            'status' => 'success'
        ), 200);
        
    }
    
    public function show(Request $request, $id)
    {
            $books = Book::find($id)
                    ->load('User')
                    ->load('Genre');
            
            return response()->json(array(
                'books' => $books,
                'status' => 'success'
            ), 200);
    }
    
    public function store(Request $request)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Datos por POST
            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);
            
            //Conseguir el usuario identificado
            $user = $jwtAuth->checkToken($hash, true);
            
            //FALTA METER IMAGEN Y URL
            $validate = \Validator::make($params_array , [
                'title' => 'required',
                'autor' => 'required',
                'description' => 'required',
                'bookimage' => 'required',
                'bookurl' => 'required',
                'genre_id' => 'required'
            ]);
            
            if($validate->fails())
            {
                return response()->json($validate->errors(), 400);
            }
            
            //RECOGER VALORES
         
            $title = $params->title;
            $autor = $params->autor;
            $description = $params->description;
            $bookimage = $params->bookimage;
            $bookurl = $params->bookurl;
            $user_id = $user->sub;
            $genre_id = $params->genre_id;
            
            
            //ASIGNAR NOMBRE Y GUARDAR PORTADA Y LIBRO EN CARPETA PUBLICA
            if(strpos($bookimage, 'data:image/png;base64') !== false)
            {
                $bookimage = str_replace('data:image/png;base64,', '', $bookimage);
                $bookimage = str_replace(' ', '+', $bookimage);
                $imageName = str_random(10).'.'.'png';
                \File::put(public_path(). '/cover/' . $imageName, base64_decode($bookimage));
            }

            if(strpos($bookimage, 'data:image/jpeg;base64') !== false)
            {
                $bookimage = str_replace('data:image/jpeg;base64,', '', $bookimage);
                $bookimage = str_replace(' ', '+', $bookimage);
                $imageName = str_random(10).'.'.'jpg';
                \File::put(public_path(). '/cover/' . $imageName, base64_decode($bookimage));
            }
            
            if(strpos($bookurl, 'data:application/octet-stream;base64') !== false)
            {
                $bookurl = str_replace('data:application/octet-stream;base64,', '', $bookurl);
                $bookurl = str_replace(' ', '+', $bookurl);
                $urlName = str_random(10).'.'.'doc';
                \File::put(public_path(). '/books/' . $urlName, base64_decode($bookurl));
            }
            
            if(strpos($bookurl, 'data:application/pdf;base64') !== false)
            {
                $bookurl = str_replace('data:application/pdf;base64,', '', $bookurl);
                $bookurl = str_replace(' ', '+', $bookurl);
                $urlName = str_random(10).'.'.'pdf';
                \File::put(public_path(). '/books/' . $urlName, base64_decode($bookurl));
            }
            
             
            
            
            //CREAR OBJETO Y GUARDARLO
            $book = new Book();
            $book->title = $title;
            $book->autor = $autor;
            $book->description = $description;
            $book->bookimage = $imageName;
            $book->bookurl = $urlName;
            $book->user_id = $user_id;
            $book->genre_id = $genre_id;
            
            $book->save();
            
            
            $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => '¡Libro registrado con exito!'
                );
            
            DB::select('call log(?,?,?)',array($user_id, $user->role, "Subida Libro"));
        }
        else
        {
           $data = array(
                'message' => 'No se ha podido registrar el libro',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
        
    }
    
    public function update(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            //Recoger Datos por POST
            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);
                 
            //FALTA METER IMAGEN Y URL
            $validate = \Validator::make($params_array , [
                'title' => 'required|min:5',
                'autor' => 'required|min:7',
                'description' => 'required|min:7',
                'genre_id' => 'required'
            ]);
            
            if($validate->fails())
            {
                return response()->json($validate->errors(), 400);
            }
          
            //Actualizar el coche
            $book = Book::where('id', $id)->update($params_array);
            
            $data = array(
                'book' => $params,
                'status' => 'success',
                'code' => 200
            );

        }
        else
        {
            //Devolver Error
            echo("NO AUTENTICADO");
            die();
        }
        
        return response()->json($data, 200);
    }
    
    public function destroy(Request $request, $id)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            $book = Book::find($id);
            
            $file_cover = public_path(). '/cover/' . $book->bookimage;
            unlink($file_cover);
            
            $file_book = public_path(). '/books/' . $book->bookurl;
            unlink($file_book);

            $book->delete();
            
            $data = array(
                'book' => $book,
                'status' => 'success',
                'code' => 200
            );
            
            DB::select('call log(?,?,?)',array(1, 'ROLE_ADMIN', "Borrar Libro"));
        }
        else
        {
            $data = array(
                'status' => 'error',
                'message' => 'Eliminación incorrecta',
                'code' => 400
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function descargar($id)
    {
        $books = Book::find($id);
        return response()->download(public_path('books/' . $books->bookurl));       
    }
    
    public function stream($id)
    {
        $books = Book::find($id);
        return response()->file(public_path('books/' . $books->bookurl));  
    }
    
    public function findbook($tipo, $nombre)
    {
        $books = new GoogleBooksApi(['key' => 'AIzaSyA8AP7ByM2XpWG9f7lTjcQljWyCrobydEg']);
        
        if($tipo == 'titulo')
        {
            $volume = $books->title($nombre);
        }
        
        if($tipo == 'autor')
        {
            $volume = $books->author($nombre);
        }
        
        if($tipo == 'isbn')
        {
            $volume = $books->isbn($nombre);
        }
        
        $lista = $volume->limit(10);
        $lista->language('es');
        $total = collect($lista);
        $total->toArray();
        
        $json = json_decode($total);

        $data = array(
                'book' => $json,
                'status' => 'success',
                'code' => 200
            );
        
         return response()->json($data, 200);
    }
    
    public function orderbook($tipo, $orden)
    {
        $books = Book::orderBy($tipo, $orden)
                ->get()
                ->load('User')
                ->load('Like')
                ->load('Genre');
        return response()->json(array(
            'books' => $books,
            'status' => 'success'
        ), 200);
    }
    
    public function storepagenumber(Request $request)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {   
            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);
            
            //Conseguir el usuario identificado
            $user = $jwtAuth->checkToken($hash, true);
            
            $idbook = $params->book_id;
            
            $recordPage = RecordPage::where(['user_id' => $params->user_id, 'book_id' => $idbook])->first();
            
            if(!is_null($recordPage))
            {
                $recordPage->numpages = $params->numpages;
                $recordPage->user_id = $params->user_id;
                $recordPage->book_id = $params->book_id;
                
                $recordPage->update();
                
                
                $data = array(
                'recordPage' => $recordPage,
                'status' => 'success',
                'code' => 200
            );
            }
            else
            {
                $nuevoRecord = new RecordPage();
                
                $nuevoRecord->numpages = $params->numpages;
                $nuevoRecord->user_id = $params->user_id;
                $nuevoRecord->book_id = $params->book_id;
                $nuevoRecord->save();
                
                $data = array(
                'recordPage' => $nuevoRecord,
                'status' => 'success',
                'code' => 200
                    );
            }
                    
        }
        else
        {
             $data = array(
                'message' => 'No se ha podido registrar las paginas',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function getpagenumber(Request $request, $identidad, $libro)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {
            $recordPage = RecordPage::where(['user_id' => $identidad, 'book_id' => $libro])->first();
            
            if(!is_null($recordPage))
            {                
                $data = array(
                'recordPage' => $recordPage,
                'status' => 'success',
                'code' => 200
                );
            }
            else
            {
                 $data = array(
                'recordPage' => 0,
                'status' => 'success',
                'code' => 200
                );
            }
        }
        else
        {
            $data = array(
                'message' => 'No se ha podido obtener las paginas',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
    }
    
    public function deletepagenumber(Request $request)
    {
        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        
        $checkToken = $jwtAuth->checkToken($hash);
        
        if($checkToken)
        {   
            $json = $request->input('json', null);
            $params = json_decode($json);
            $idbook = $params->book_id;
            
            $recordPage = RecordPage::where(['user_id' => $params->user_id, 'book_id' => $idbook])->first();
            $recordPage->delete();
            
            $data = array(
                'recordPage' => $recordPage,
                'status' => 'success',
                'code' => 300
            );
            
        }
        else
        {
             $data = array(
                'message' => 'No se ha podido borrar las paginas',
                'status' => 'error',
                'code' => 300
            );
        }
        
        return response()->json($data, 200);
    }
}

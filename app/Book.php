<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books'; 
    
    
    //Relaciones
    
    //Relaciones Muchos a Uno
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    
    //Relaciones Muchos a Uno
    public function genre()
    {
        return $this->belongsTo('App\Genre', 'genre_id');
    }
    
    //Relaciones Uno a Muchos
    public function comment()
    {
        return $this->hasMany('App\Comment', 'book_id');
    }
    
     //Relaciones Uno a Muchos
    public function like()
    {
        return $this->hasMany('App\Like', 'book_id');
    }
     
}

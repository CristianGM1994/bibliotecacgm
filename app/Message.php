<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';
    
    //Relaciones Muchos a Uno
    public function user()
    {
        return $this->belongsTo('App\User', 'user_send');
    }
    
    public function userreceive()
    {
        return $this->belongsTo('App\User', 'user_receive');
    }
}
